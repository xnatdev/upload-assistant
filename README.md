# XNAT Upload Assistant #

The XNAT upload assistant provides upload functionality for transferring DICOM and ECAT imaging data into XNAT servers.

## Building the Upload Assistant

The full build requires [install4j](https://www.ej-technologies.com/products/install4j/overview.html) to be installed and your Maven settings to be [properly configured to work with install4j](http://sonatype.github.io/install4j-support/install4j-maven-plugin/usage.html).

Build the upload assistant installers with the following command:

```bash
mvn clean package -DskipTests=true \
    -Dapp.updateRepo=https://server/path/to/updates.xml \
    -Dsigning.winKeystore=/path/to/signing/cert \
    -Dsigning.macKeystore=/path/to/signing/cert \
    -Dsigning.keystore=/path/to/keystore \
    -Dsigning.alias=<alias> \
    -Dsigning.storepass=<password> \
    -Dsigning.keypass=<password>
```

