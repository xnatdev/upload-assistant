/*
 * upload-assistant: org.nrg.xnat.upload.ui.SwingUploadFailureHandlerTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.finder.JOptionPaneFinder;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.junit.testcase.FestSwingJUnitTestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class SwingUploadFailureHandlerTest extends FestSwingJUnitTestCase {
    private static final int    N_FILES = 4;
    private              File[] files   = new File[N_FILES];
    private FrameFixture fixture;

    @Before
    public void setUpFiles() throws IOException {
        for (int i = 0; i < N_FILES; i++) {
            files[i] = File.createTempFile("SwingUploadFailureHandler", "test");
            files[i].deleteOnExit();
        }
    }

    protected void onSetUp() {
        Frame frame = GuiActionRunner.execute(new GuiQuery<Frame>() {
            protected Frame executeInEDT() {
                return new Frame("Unit test frame");
            }
        });
        fixture = new FrameFixture(robot(), frame);
        fixture.show(); // shows the frame to test
    }

    @After
    public void removeFiles() {
        fixture.cleanUp();
        for (int i = 0; i < N_FILES; i++) {
            files[i].delete();
            files[i] = null;
        }
    }

    interface Returner<T> {
        T getValue();
    }

    interface RunnableReturner<T> extends Runnable, Returner<T> {
    }

    private void waitForDialog() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignore) {
        }
    }

    /**
     * Test method for {@link org.nrg.xnat.upload.ui.SwingUploadFailureHandler#shouldRetry(Object, Object)}.
     */
    @Test
    public void testHandleSecondFailure() throws InterruptedException, InvocationTargetException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException {
        for (boolean shouldRetry : new Boolean[]{true, false}) {
            final SwingUploadFailureHandler handler = new SwingUploadFailureHandler(fixture.component(), 1);
            assertEquals(true, handler.shouldRetry(files[0], mock(IOException.class)));

            final RunnableReturner<Boolean> returner =
                    new RunnableReturner<Boolean>() {
                        private Boolean result;

                        public void run() {
                            result = handler.shouldRetry(files[0], mock(IOException.class));
                        }

                        public Boolean getValue() {
                            return result;
                        }
                    };
            SwingUtilities.invokeLater(returner);

            waitForDialog();

            final String buttonText = handler.getButtonTextFor(shouldRetry);

            JOptionPaneFinder.findOptionPane().using(robot()).buttonWithText(buttonText).click();
            assertEquals(shouldRetry, returner.getValue());
        }
    }

    /**
     * Test method for {@link org.nrg.xnat.upload.ui.SwingUploadFailureHandler#shouldRetry(Object, Object)}.
     */
    @Test
    public void testHandleMixedFailures() throws InterruptedException, InvocationTargetException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException {
        final SwingUploadFailureHandler handler = new SwingUploadFailureHandler(fixture.component(), 3);
        assertEquals(true, handler.shouldRetry(files[0], mock(IOException.class)));
        assertEquals(true, handler.shouldRetry(files[0], mock(IOException.class)));
        assertEquals(true, handler.shouldRetry(files[1], "just didn't feel like it"));

        final RunnableReturner<Boolean> returner =
                new RunnableReturner<Boolean>() {
                    private Boolean result;

                    public void run() {
                        result = handler.shouldRetry(files[0], mock(IOException.class));
                    }

                    public Boolean getValue() {
                        return result;
                    }
                };
        SwingUtilities.invokeLater(returner);

        waitForDialog();

        JOptionPaneFinder.findOptionPane().using(robot()).buttonWithText(handler.getButtonTextFor(false)).click();
        assertEquals(false, returner.getValue());
    }
}
