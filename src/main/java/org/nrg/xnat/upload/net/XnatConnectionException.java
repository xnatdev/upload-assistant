/*
 * upload-assistant: org.nrg.xnat.upload.net.XnatConnectionException
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net;

import java.net.URL;

/**
 * Used to indicate the cause of failures to connect to an XNAT server.
 */
public class XnatConnectionException extends Exception {
    public enum Failure {
        EmptyAddress,
        UnverifiedHttps,
        SSLProtocol,
        InvalidAddress,
        NotAnXnatServer,
        SystemError,
        AuthenticationFailed,
        Unauthorized,
        Unknown,
        Unsupported;
    }
    public XnatConnectionException(final Failure failure) {
        _failure = failure;
        _url = null;
    }

    public XnatConnectionException(final Failure failure, final String message) {
        super(message);
        _failure = failure;
        _url = null;
    }

    public XnatConnectionException(final Failure failure, final URL url) {
        _failure = failure;
        _url = url;
    }

    public XnatConnectionException(final Failure failure, final URL url, final Throwable throwable) {
        super(throwable);
        _failure = failure;
        _url = url;
    }

    public XnatConnectionException(final Failure failure, final String message, final Throwable throwable) {
        super(message, throwable);
        _failure = failure;
        _url = null;
    }

    public Failure getFailure() {
        return _failure;
    }

    public URL getUrl() {
        return _url;
    }

    private final Failure _failure;
    private final URL _url;
}
