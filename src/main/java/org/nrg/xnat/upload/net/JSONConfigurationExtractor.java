package org.nrg.xnat.upload.net;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Turns a JSON object from the XNAT configuration service into a map. Before calling the {@link #decode(org.json.JSONObject)}
 * method, you can call the {@link #put(Object, Object)} method to set default values for one or more fields in the
 * configuration.
 */
final class JSONConfigurationExtractor extends HashMap<String, Object> implements JSONDecoder {
    @Override
    public void decode(final JSONObject jsonObject) throws JSONException {
        final Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            final String key = (String) keys.next();
            put(key, jsonObject.get(key));
        }
        if (containsKey("status") && !get("status").equals("enabled")) {
            put("status", "disabled");
            return;
        }
        if (containsKey("contents")) {
            final String contents = (String) get("contents");
            try {
                JSONObject translated = new JSONObject(new JSONTokener(contents));
                Map<String, Object> decoded = new HashMap<>();
                final Iterator contentKeys = translated.keys();
                while (contentKeys.hasNext()) {
                    final String key = (String) contentKeys.next();
                    decoded.put(key, translated.has(key) ? translated.get(key) : "");
                }
                put("contents", decoded);
            } catch (JSONException ignored) {
                // If we get a JSONException, then the contents isn't something we know how to work with.
                // Leave the contents alone.
            }
        }
    }
}
