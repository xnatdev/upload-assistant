/*
 * upload-assistant: org.nrg.xnat.upload.net.StringResponseProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.netbeans.spi.wizard.ResultProgressHandle;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

@Getter
@Accessors(prefix = "_")
@Slf4j
public class StringResponseProcessor extends AbstractResponseProcessor {
	public StringResponseProcessor(final InputStream inputStream, final String mimeMediaType, final Long contentLength, final ResultProgressHandle progressHandle) {
		super(inputStream, mimeMediaType, contentLength, progressHandle);
	}
	
	public StringResponseProcessor() {
		this(null, null, null, null);
	}

	/* (non-Javadoc)
	 * @see HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
	 */
	public void prepare(final HttpURLConnection connection) throws IOException {
		if (getInputStream() == null) {
			return;
		}
		super.prepare(connection);
	}

	/* (non-Javadoc)
	 * @see HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
	 */
	public void process(final HttpURLConnection connection) throws IOException {
		try (final InputStream inputStream = connection.getInputStream()) {
			final byte[] buffer = new byte[BUF_SIZE];
			for (int read = inputStream.read(buffer); read > 0; read = inputStream.read(buffer)) {
				getBuffer().append(new String(buffer, 0, read));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() { return getBuffer().toString(); }

	private static final int BUF_SIZE = 512;

	private final StringBuilder _buffer = new StringBuilder();
}
