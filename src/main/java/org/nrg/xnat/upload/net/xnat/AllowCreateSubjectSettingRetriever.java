package org.nrg.xnat.upload.net.xnat;

public final class AllowCreateSubjectSettingRetriever extends AbstractBooleanSettingRetriever {
    public AllowCreateSubjectSettingRetriever(final String project) {
        super(project);
    }

    @Override
    protected boolean getDefaultReturnValue() {
        return true;
    }

    @Override
    protected String getProjectUrlTemplate() {
        return URL_TEMPLATE_PROJECT;
    }

    @Override
    protected String getSiteUrl() {
        return URL_TEMPLATE_SITE;
    }

    private static final String URL_TEMPLATE_SITE    = "/data/config/applet/allow-create-subject?contents=true&accept-not-found=true";
    private static final String URL_TEMPLATE_PROJECT = "/data/projects/%s/config/applet/allow-create-subject?contents=true&accept-not-found=true";
}
