/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.ProjectSessionLister
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.JSONDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class ProjectSessionLister implements Callable<ProjectSessionCollection> {
    public ProjectSessionLister(final String project) {
        _path = String.format(URL_TEMPLATE, project);
    }

    public ProjectSessionCollection call() throws IOException, JSONException {
        try {
            final ProjectSessionExtractor extractor = new ProjectSessionExtractor();
            getRestServer().doGet(_path, extractor);
            return extractor.getSessions();
        } catch (HttpException e) {
            if (e.getResponseCode() == 404) {
                _log.warn("Got a 404 calling {}. This may mean there are no sessions, but it may also mean something went wrong on the server.", _path);
                return new ProjectSessionCollection();
            } else {
                throw e;
            }
        }
    }

    private static final class ProjectSessionExtractor implements JSONDecoder {
        public void decode(final JSONObject jsonObject) throws JSONException {
            final ProjectSession projectSession = new ProjectSession();
            projectSession.setId(jsonObject.optString("ID"));
            projectSession.setLabel(jsonObject.optString("label"));
            projectSession.setStatus(jsonObject.optString("xnat:experimentdata/meta/status"));
            _sessions.add(projectSession);
        }

        public ProjectSessionCollection getSessions() {
            return _sessions;
        }

        private ProjectSessionCollection _sessions = new ProjectSessionCollection();
    }

    private static final String URL_TEMPLATE = "/data/projects/%s/experiments?columns=ID,label,xnat:experimentData/meta/status&format=json";

    private static final Logger _log = LoggerFactory.getLogger(ProjectSessionLister.class);

    private final String _path;
}
