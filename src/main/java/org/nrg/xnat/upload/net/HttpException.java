/*
 * upload-assistant: org.nrg.xnat.upload.net.HttpException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class HttpException extends IOException {
    /**
     * Creates a new exception object containing relevant data for the error.
     *
     * @param responseCode    The response code that occurred.
     * @param responseMessage The message that accompanied the response.
     * @param operation       The operation that caused the exception in the form METHOD URL.
     */
    public HttpException(final int responseCode, final String responseMessage, final String operation) {
        this(responseCode, responseMessage, operation, null);
    }

    /**
     * Creates a new exception object containing relevant data for the error.
     *
     * @param responseCode    The response code that occurred.
     * @param responseMessage The message that accompanied the response.
     * @param operation       The operation that caused the exception in the form METHOD URL.
     * @param entity          The entity returned with the HTTP response.
     */
    public HttpException(final int responseCode, final String responseMessage, final String operation, final String entity) {
        super(buildMessage(responseCode, responseMessage, operation));
        _responseCode = responseCode;
        _operation = operation;
        _entity = entity;
    }

    /**
     * Gets the code from the HTTP response.
     *
     * @return The response code.
     */
    public int getResponseCode() {
        return _responseCode;
    }

    /**
     * Gets the operation that caused the exception.
     *
     * @return The operation in the form METHOD URL.
     */
    public String getOperation() {
        return _operation;
    }

    /**
     * Gets the entity (or body) from the HTTP response if available and provided.
     *
     * @return The response entity.
     */
    public String getEntity() {
        return _entity;
    }

    /**
     * Indicates whether the exception thinks the response entity has useful information for the application user.
     * "Useful" in this case means not blank, not an empty JSON entity (i.e. "{}" or "[]"), and not an HTML page.
     *
     * @return Whether the response entity appears to contain information useful to the application user.
     */
    public boolean isUsefulEntity() {
        return StringUtils.isNotBlank(_entity) && !StringUtils.equals("{}", _entity) && !StringUtils.equals("[]", _entity) && !StringUtils.contains(_entity, "<html");
    }

    private static String buildMessage(final int code, final String message, final String text) {
        final StringBuilder buffer = new StringBuilder("HTTP status ");
        buffer.append(code);
        if (null != message && !"".equals(message)) {
            buffer.append("(").append(message).append(")");
        }
        if (null != text && !"".equals(text)) {
            buffer.append(": ").append(text);
        }
        return buffer.toString();
    }

    private final int    _responseCode;
    private final String _operation;
    private final String _entity;
}
