package org.nrg.xnat.upload.net;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Provides an abstract interface for providing JSON deserialization.
 */
public interface JSONDecoder {
    /**
     * Decode the indicated object. The meaning of "decode" depends on the implementation.
     *
     * @param jsonObject The JSON object to be decoded.
     *
     * @throws JSONException When an error occurs parsing or deserializing the JSON object.
     */
    void decode(final JSONObject jsonObject) throws JSONException;
}
