/*
 * upload-assistant: org.nrg.xnat.upload.net.NotFoundHttpException
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net;

import java.net.HttpURLConnection;

/**
 * Provides easier handling of not found exceptions.
 */
public class NotFoundHttpException extends HttpException {
    public NotFoundHttpException(final String operation) {
        super(CODE, MESSAGE, operation);
    }

    private static final int    CODE    = HttpURLConnection.HTTP_NOT_FOUND;
    private static final String MESSAGE = "Not Found";
}
