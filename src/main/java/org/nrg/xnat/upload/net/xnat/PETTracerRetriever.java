/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.PETTracerRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import com.google.common.collect.Sets;
import org.nrg.xnat.upload.net.StringListConnectionProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class PETTracerRetriever implements Callable<Set<String>> {
    public PETTracerRetriever(final String project) {
        _projectPath = String.format(URL_TEMPLATE_PROJECT, project);
    }

    public Set<String> call() {
        final Set<String> project = getTracers(_projectPath);
        if (project != null && project.size() > 0) {
            return project;
        }
        final Set<String> site = getTracers(URL_TEMPLATE_SITE);
        if (site != null && site.size() > 0) {
            return site;
        }
        return getDefaultTracers();
    }

    public static Set<String> getDefaultTracers() {
        return Sets.newLinkedHashSet(DEFAULT_TRACERS);
    }

    private Set<String> getTracers(final String path) {
        final StringListConnectionProcessor processor = new StringListConnectionProcessor();
        try {
            getRestServer().doGet(path, processor);
            return new HashSet<>(processor.getStrings());
        } catch (Throwable t) {
            _log.warn("An error occurred trying to retrieve tracers from the path " + path, t);
            return null;
        }
    }

    private static Set<String> getDefaultTracers(final String resource) {
        try (final InputStream input = PETTracerRetriever.class.getResourceAsStream(resource)) {
            if (null == input) {
                throw new RuntimeException("Unable to load default PET tracers");
            }
            return Sets.newLinkedHashSet(StringListConnectionProcessor.readStrings(input));
        } catch (IOException e) {
            throw new RuntimeException("Unable to read default PET tracers", e);
        }
    }

    private static final String      URL_TEMPLATE_SITE        = "/data/config/tracers/tracers?contents=true&accept-not-found=true";
    private static final String      URL_TEMPLATE_PROJECT     = "/data/projects/%s/config/tracers/tracers?contents=true&accept-not-found=true";
    private static final String      DEFAULT_TRACERS_RESOURCE = "/PET-tracers.txt";
    private static final Set<String> DEFAULT_TRACERS          = getDefaultTracers(DEFAULT_TRACERS_RESOURCE);

    private static final Logger _log = LoggerFactory.getLogger(PETTracerRetriever.class);

    private final String _projectPath;
}
