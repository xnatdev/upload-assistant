/*
 * upload-assistant: org.nrg.xnat.upload.net.ConflictHttpException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

public class ConflictHttpException extends HttpException {
	private static final long serialVersionUID = 1L;
	
	private static final int CODE = 409;
	private static final String MESSAGE = "Conflict";
	
	/**
	 * Creates an HTTP exception for resource conflicts.
	 * @param message     The message to be displayed for the error.
	 */
	public ConflictHttpException(final String message) {
		super(CODE, MESSAGE, message);
	}
}
