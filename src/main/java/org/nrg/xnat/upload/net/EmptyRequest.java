/*
 * upload-assistant: org.nrg.xnat.upload.net.EmptyRequest
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import java.io.IOException;
import java.net.HttpURLConnection;


class EmptyRequest implements HttpURLConnectionProcessor {
	/*
	 * (non-Javadoc)
	 * @see HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
	 */
	public void prepare(final HttpURLConnection connection) {}
	
	/*
	 * (non-Javadoc)
	 * @see HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
	 */
	public void process(final HttpURLConnection connection) throws IOException {
		try {
		} finally {
			connection.disconnect();
		}
	}
}