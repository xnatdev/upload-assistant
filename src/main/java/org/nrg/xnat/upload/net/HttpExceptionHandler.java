/*
 * upload-assistant: org.nrg.xnat.upload.net.HttpExceptionHandler
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.concurrency.AbstractExceptionHandler;
import org.nrg.framework.concurrency.LoggingThreadPoolExecutor;
import org.nrg.framework.exceptions.NrgRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Handles HTTP exceptions rather than using the default {@link LoggingThreadPoolExecutor} handling. If no handled
 * response codes are configured, this handler handles all {@link HttpException} objects regardless of the response
 * code associated with the exception. If any response codes are configured as handled, this handler only handles those
 * exceptions that have a response code contained in the list of handled response codes.
 */
public class HttpExceptionHandler extends AbstractExceptionHandler {
    /**
     * Creates an HttpException handler that handles all response codes.
     */
    public HttpExceptionHandler() {
        _handledResponseCodes = null;
    }

    /**
     * Creates an HttpException handler that handles the submitted response codes.
     *
     * @param first  The first response code to handle.
     * @param others Any other response codes to handle.
     */
    public HttpExceptionHandler(final Integer first, final Integer... others) {
        this(Lists.asList(first, others));
    }

    /**
     * Creates an HttpException handler that handles the submitted response codes.
     *
     * @param codes The response codes to handle.
     */
    public HttpExceptionHandler(final List<Integer> codes) {
        _handledResponseCodes = codes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean handles(final Throwable throwable) {
        return throwable instanceof HttpException && isHandledResponseCode((HttpException) throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(final Throwable throwable, final Logger logger) {
        // This shouldn't happen because the caller should check first, but just in case...
        if (!(throwable instanceof HttpException)) {
            throw new NrgRuntimeException("A throwable of type " + throwable.getClass().getName() + " was submitted to the HttpExceptionHandler but does not meet this handler's criteria. You should always call the ExceptionHandler.handles() method before calling this method.");
        }
        final HttpException httpException = (HttpException) throwable;
        if (!handles(throwable)) {
            throw new NrgRuntimeException("An HttpException was submitted to the HttpExceptionHandler but does not meet this handler's criteria. It probably doesn't have an appropriate response code. Yours has: " + httpException.getResponseCode() + ", while this handler handles: " + Joiner.on(", ").join(_handledResponseCodes) + ". You should always call the ExceptionHandler.handles() method before calling this method.");
        }
        log(logger != null ? logger : _log, httpException);
    }

    private boolean isHandledResponseCode(final HttpException throwable) {
        return _handledResponseCodes == null || _handledResponseCodes.contains(throwable.getResponseCode());
    }

    private void log(final Logger logger, final HttpException exception) {
        final StringBuilder buffer = new StringBuilder("An HTTP operation got the response code: ");
        buffer.append(exception.getResponseCode());
        final String message = exception.getMessage();
        if (StringUtils.isBlank(message)) {
            buffer.append(".");
        } else {
            buffer.append(": ").append(message);
            if (!message.endsWith(".")) {
                buffer.append(".");
            }
        }
        log(logger, buffer.toString());
    }

    private static final Logger _log = LoggerFactory.getLogger(HttpExceptionHandler.class);

    private final List<Integer> _handledResponseCodes;
}
