/*
 * upload-assistant: org.nrg.xnat.upload.net.AbstractResponseProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2018, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.nrg.IOUtils;
import org.nrg.xnat.upload.ui.ResultProgressListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

@Getter(AccessLevel.PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public abstract class AbstractResponseProcessor implements HttpURLConnectionProcessor {
    protected AbstractResponseProcessor(final InputStream inputStream, final String mimeMediaType, final Long contentLength, final ResultProgressHandle progressHandle) {
        if (contentLength != null && contentLength < 0) {
            throw new IllegalArgumentException("content length = " + contentLength + "; must be > 0");
        }
        _inputStream = inputStream;
        _mimeMediaType = mimeMediaType;
        _contentLength = contentLength == null ? -1 : contentLength;
        _progressListener = contentLength == null ? null : new ResultProgressListener(progressHandle, 0, contentLength);
    }

    /*
     * (non-Javadoc)
     * @see HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
     */
    public void prepare(final HttpURLConnection connection) throws IOException {
        connection.setDoOutput(true);
        if (getMimeMediaType() != null) {
            connection.addRequestProperty(CONTENT_TYPE_HEADER, _mimeMediaType);
        }
        if (getContentLength() >= 0) {
            connection.setFixedLengthStreamingMode(_contentLength);
        }
        connection.connect();
        try {
            try (final OutputStream outputStream = connection.getOutputStream()) {
                IOUtils.copy(outputStream, getInputStream(), getProgressListener());
                outputStream.flush();
            } catch (Throwable t) {
                log.debug("stream copy failed", t);
            }
        } finally {
            getInputStream().close();
        }
    }

    private static final String CONTENT_TYPE_HEADER = "Content-Type";

    private final InputStream            _inputStream;
    private final String                 _mimeMediaType;
    private final long                   _contentLength;
    private final ResultProgressListener _progressListener;
}
