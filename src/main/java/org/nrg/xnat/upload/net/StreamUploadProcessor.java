/*
 * upload-assistant: org.nrg.xnat.upload.net.StreamUploadProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import lombok.extern.slf4j.Slf4j;
import org.netbeans.spi.wizard.ResultProgressHandle;

import java.io.InputStream;
import java.net.HttpURLConnection;

@Slf4j
public class StreamUploadProcessor extends AbstractResponseProcessor {
	public StreamUploadProcessor(final InputStream inputStream, final String mimeMediaType, final Long contentLength, final ResultProgressHandle progressHandle) {
		super(inputStream, mimeMediaType, contentLength, progressHandle);
	}
	
	/* (non-Javadoc)
	 * @see HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
	 */
	public final void process(final HttpURLConnection connection) {}
}
