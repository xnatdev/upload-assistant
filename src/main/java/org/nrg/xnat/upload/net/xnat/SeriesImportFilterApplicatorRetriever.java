/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.SeriesImportFilterApplicatorRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.json.JSONException;
import org.nrg.dicomtools.filters.DicomFilterService;
import org.nrg.dicomtools.filters.RegExBasedSeriesImportFilter;
import org.nrg.dicomtools.filters.SeriesImportFilter;
import org.nrg.xnat.upload.net.HttpException;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class SeriesImportFilterApplicatorRetriever {

    private static final String SITE_WIDE_PATH = "/data/config/seriesImportFilter/config?format=json";
    private static final String PROJECT_SPECIFIC_PATH = "/data/projects/%s/config/seriesImportFilter/config?format=json";
    private static final String SPLIT_PETMR_SESSIONS_PATH = "/data/automation/scripts/SplitPetMrSessions";
    private static final String SPLIT_PETMR_SITE_SETTING_PATH = "/data/config/separatePETMR/config";
    private static final String SPLIT_PETMR_PROJECT_SETTING_PATH = "/data/projects/%s/config/separatePETMR/config";
    private static final RegExBasedSeriesImportFilter NULL_FILTER;
    static {
        try {
            NULL_FILTER = new RegExBasedSeriesImportFilter("{\"mode\": \"blacklist\", \"list\": \"\"}");
        } catch (IOException e) {
            throw new RuntimeException("Error initializing the NULL filter", e);
        }
    }

    private final SeriesImportFilter _siteWideFilter;
    private final SeriesImportFilter _projectFilter;
    private final SeriesImportFilter _splitPetMrSessionsFilter;
    private final boolean _separatePetMr;

	public SeriesImportFilterApplicatorRetriever(final String project) throws Exception {
        _siteWideFilter = extractSeriesImportFilters(SITE_WIDE_PATH);
        if (StringUtils.isNotBlank(project)) {
            _projectFilter = extractSeriesImportFilters(String.format(PROJECT_SPECIFIC_PATH, project));
        } else {
            _projectFilter = null;
        }
        _separatePetMr = getSplitPetMrSetting(project);
        if (_separatePetMr) {
            _splitPetMrSessionsFilter = extractSeriesImportFilters(SPLIT_PETMR_SESSIONS_PATH);
            if (_splitPetMrSessionsFilter == null) {
                throw new RuntimeException("The system or project setting indicates that PET/MR sessions should be split, but there is no PET/MR session filter configured. Please contact your system administrator.");
            }
        } else {
            _splitPetMrSessionsFilter = null;
        }
    }

    public boolean shouldIncludeDicomObject(final DicomObject object) {
        return _siteWideFilter.shouldIncludeDicomObject(object) && (_projectFilter == null || _projectFilter.shouldIncludeDicomObject(object));
    }

    public boolean shouldSeparatePetMr() {
        return _separatePetMr;
    }

    public String findModality(final DicomObject object) {
        // The split
        if (_separatePetMr && _splitPetMrSessionsFilter != null) {
            return _splitPetMrSessionsFilter.findModality(object);
        }
        final String siteWideModality = _siteWideFilter.findModality(object);
        final String projectModality = _projectFilter == null ? "" : _projectFilter.findModality(object);
        if (StringUtils.isNotBlank(projectModality)) {
            return projectModality;
        }
        if (StringUtils.isNotBlank(siteWideModality)) {
            return siteWideModality;
        }
        return null;
    }

    private SeriesImportFilter extractSeriesImportFilters(final String path) throws IOException, JSONException {
        try {
            final LinkedHashMap<String, String> keys = getRestServer().getSeriesImportFilter(path);
            return DicomFilterService.buildSeriesImportFilter(keys);
        } catch (HttpException exception) {
            // If it's a 404, that's totally cool, it can not exist.
            if (exception.getResponseCode() == 404) {
                // Return the null filter.
                return NULL_FILTER;
            }
            throw exception;
        }
    }

    private boolean getSplitPetMrSetting(String project) throws Exception {
        final Map<String, String> projectConfig = getRestServer().getConfigurationProperties(String.format(SPLIT_PETMR_PROJECT_SETTING_PATH, project), "status", "contents");
        return isSeparateConfig(projectConfig) || isSeparateConfig(getRestServer().getConfigurationProperties(SPLIT_PETMR_SITE_SETTING_PATH, "status", "contents"));
    }

    private boolean isSeparateConfig(final Map<String, String> config) {
        return config != null && StringUtils.equalsIgnoreCase(config.get("status"), "enabled") && StringUtils.equalsIgnoreCase(config.get("contents"), "separate");
    }
}
