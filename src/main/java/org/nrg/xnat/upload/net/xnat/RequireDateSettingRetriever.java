package org.nrg.xnat.upload.net.xnat;

public final class RequireDateSettingRetriever extends AbstractBooleanSettingRetriever {
    public RequireDateSettingRetriever(final String project) {
        super(project);
    }

    @Override
    protected boolean getDefaultReturnValue() {
        return false;
    }

    @Override
    protected String getProjectUrlTemplate() {
        return URL_TEMPLATE_PROJECT;
    }

    @Override
    protected String getSiteUrl() {
        return URL_TEMPLATE_SITE;
    }

    private static final String URL_TEMPLATE_SITE    = "/data/config/applet/require-date?contents=true&accept-not-found=true";
    private static final String URL_TEMPLATE_PROJECT = "/data/projects/%s/config/applet/require-date?contents=true&accept-not-found=true";
}
