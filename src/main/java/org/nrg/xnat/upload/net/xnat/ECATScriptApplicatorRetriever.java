/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.ECATScriptApplicatorRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.ecat.edit.ScriptApplicator;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.Callable;

public final class ECATScriptApplicatorRetriever extends BaseScriptApplicatorRetriever<ScriptApplicator> implements Callable<ScriptApplicator> {
    public ECATScriptApplicatorRetriever(final String project, final Map<String, ? extends ScriptFunction> scriptFunctions) {
        super(buildFactory(scriptFunctions), "/data/projects/" + project + "/resources/UPLOAD_CONFIG/files/ecat.eas?accept-not-found=true");
    }

    private static ScriptApplicatorFactory<ScriptApplicator> buildFactory(final Map<String, ? extends ScriptFunction> scriptFunctions) {
        return new ScriptApplicatorFactory<ScriptApplicator>() {
            public ScriptApplicator createScriptApplicator(final InputStream stream) throws IOException, ScriptEvaluationException {
                return new ScriptApplicator(stream, scriptFunctions);
            }
        };
    }
}
