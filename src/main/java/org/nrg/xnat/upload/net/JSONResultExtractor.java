/*
 * upload-assistant: org.nrg.xnat.upload.net.JSONResultExtractor
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import org.json.JSONArray;
import org.json.JSONException;

class JSONResultExtractor implements HttpURLConnectionProcessor {
	private final JSONDecoder _decoder;

	JSONResultExtractor(final JSONDecoder decoder) {
		_decoder = decoder;
	}

	/**
	 * {@inheritDoc}
	 */
	public void prepare(final HttpURLConnection connection) {}
	
	/**
	 * {@inheritDoc}
	 */
	public void process(final HttpURLConnection connection) throws IOException, JSONException {
		try (final InputStream inputStream = connection.getInputStream()) {
			final JSONArray entries = RestServer.extractResultFromEntity(RestServer.extractJSONEntity(inputStream));
			for (int i = 0; i < entries.length(); i++) {
				_decoder.decode(entries.getJSONObject(i));
			}
		}
	}
}
