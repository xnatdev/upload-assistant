/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.ProjectSubjectLister
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import org.json.JSONException;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class ProjectSubjectLister implements Callable<Map<String, String>> {
    public ProjectSubjectLister(final String project) {
        _uri = String.format(URL_TEMPLATE, project);
    }

    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    public Map<String, String> call() throws IOException, JSONException {
        return getRestServer().getAliases(_uri);
    }

    public String getUri() {
        return _uri;
    }

    private static final String URL_TEMPLATE = "/data/projects/%s/subjects?format=json";

    private final String _uri;
}
