/*
 * upload-assistant: org.nrg.xnat.upload.ui.SelectSubjectPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import com.google.common.base.Joiner;
import org.json.JSONException;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.xnat.upload.application.UploadAssistant;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.data.Subject;
import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.NotFoundHttpException;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

public class SelectSubjectPage extends WizardPage {
    public static String getDescription() {
        return Messages.getPageTitle(SelectSubjectPage.class);
    }

    public SelectSubjectPage(final Dimension dimension) {
        super();

        _subjects = new JList<>();
        _subjects.setName("subject");
        _subjects.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _subjects.addMouseListener(new DoubleClickListener(this));

        _dimension = dimension;

        setLongDescription(Messages.getPageDescription(SelectSubjectPage.class));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void recycle() {
        _logger.info("Recycling SelectSubjectPage");
        _subjects.setModel(new DefaultListModel<Subject>());
        removeAll();
        validate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void renderingPage() {
        setBusy(true);
        _project = getCurrentXnatServer().getCurrentProject();

        refreshSubjectList();

        final JScrollPane subjectList = new JScrollPane(_subjects);
        if (null != _dimension) {
            subjectList.setPreferredSize(_dimension);
        }
        add(subjectList);

        if (_project != null && _project.allowCreateSubject()) {
            final JButton newSubject = new JButton("Create new subject");

            final NewSubjectDialog newSubjectDialog = new NewSubjectDialog(this, _project);
            newSubject.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    newSubjectDialog.reset();
                    newSubjectDialog.setVisible(true);
                }
            });
            newSubject.setEnabled(true);
            add(newSubject);
        }

        setBusy(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String validateContents(final Component component, final Object event) {
        if (event instanceof ListSelectionEvent) {
            getSettings().setCurrentSubject(_subjects.isSelectionEmpty() ? null : _subjects.getSelectedValue());
        }
        return getSettings().getCurrentSubject() == null ? "Select a subject to proceed" : null;
    }

    /**
     * Refreshes the list of subjects in the current project.
     *
     * @param selection item in the subjects list to be selected after refresh
     */
    void refreshSubjectList(final Subject selection) {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        getSettings().setCurrentSubject(null);
        final Callable<Subject> doRefresh = new Callable<Subject>() {
            public Subject call() throws IOException, JSONException {
                for (; ; ) {
                    _subjects.removeAll();
                    try {
                        final Collection<Subject> found = _project.getSubjects();
                        if (_logger.isDebugEnabled()) {
                            _logger.debug("Found {} subjects for the project {}: ", found.size(), _project.toString(), Joiner.on(", ").join(found));
                        } else {
                            _logger.info("Found {} subjects for the project {}", found.size(), _project.toString());
                        }

                        final SortedSet<Subject> subjects = new TreeSet<>(new Subject.SubjectComparator());
                        subjects.addAll(found);

                        if (selection == null) {
                            _logger.info("There's no selected subject (null) so the list of subjects doesn't contain that.");
                        } else if (subjects.contains(selection)) {
                            _logger.info("Found the selected subject {}" + selection.toString());
                        } else {
                            _logger.info("The list of subjects doesn't contain the selected subject.");
                        }
                        _subjects.setListData(subjects.toArray(new Subject[subjects.size()]));
                        break;
                    } catch (InterruptedException retry) {
                        _logger.info("subject retrieval interrupted, retrying", retry);
                    } catch (ExecutionException e) {
                        final Throwable cause = e.getCause();
                        if (cause instanceof NotFoundHttpException) {
                            final String operation = ((NotFoundHttpException) cause).getOperation();
                            final String message   = Messages.getDialogFormattedMessage("selectsubjectpage.notfounderror.message", getSettings().getCurrentXnatServer().getAddress());
                            JOptionPane.showMessageDialog(SelectSubjectPage.this,
                                                          message,
                                                          Messages.getMessage("selectsubjectpage.httperror.title"),
                                                          JOptionPane.ERROR_MESSAGE);
                            _logger.error("Error getting subject list from {}", operation);
                            break;
                        } else if (cause instanceof HttpException) {
                            final HttpException httpException = (HttpException) cause;
                            final String        message;
                            if (httpException.isUsefulEntity()) {
                                message = Messages.getDialogFormattedMessage("selectsubjectpage.httperrorwithmessage.message", httpException.getResponseCode(), httpException.getOperation(), httpException.getEntity());
                            } else {
                                message = Messages.getDialogFormattedMessage("selectsubjectpage.httperrorwithoutmessage.message", httpException.getResponseCode(), httpException.getOperation());
                            }
                            final Object[] options = {"Retry", "Cancel"};
                            final int n = JOptionPane.showOptionDialog(SelectSubjectPage.this,
                                                                       message,
                                                                       Messages.getMessage("selectsubjectpage.httperror.title"),
                                                                       JOptionPane.YES_NO_OPTION,
                                                                       JOptionPane.ERROR_MESSAGE,
                                                                       null,
                                                                       options,
                                                                       options[0]);
                            if (JOptionPane.NO_OPTION == n) {
                                _logger.error("error getting subject list; cancelling at user request", cause.getMessage());
                                break;
                            } else {
                                _logger.error("error getting subject list; retrying at user request", cause.getMessage());
                            }
                        } else if (cause instanceof IOException) {
                            final Object[] options = {"Retry", "Cancel"};
                            final int n = JOptionPane.showOptionDialog(SelectSubjectPage.this,
                                                                       Messages.getDialogFormattedMessage("selectsubjectpage.networkerror.message", getCurrentXnatServer().getAddress(), cause.getMessage()),
                                                                       Messages.getMessage("selectsubjectpage.networkerror.title"),
                                                                       JOptionPane.YES_NO_OPTION,
                                                                       JOptionPane.ERROR_MESSAGE,
                                                                       null,
                                                                       options,
                                                                       options[0]);
                            if (JOptionPane.NO_OPTION == n) {
                                throw new IOException(cause);
                            } else {
                                _logger.error("error getting subject list; retrying at user request", cause);
                            }
                        } else if (cause instanceof JSONException) {
                            final Object[] options = {"Retry", "Cancel"};
                            final int n = JOptionPane.showOptionDialog(SelectSubjectPage.this,
                                                                       Messages.getDialogFormattedMessage("selectsubjectpage.servererror.message", getCurrentXnatServer().getAddress()),
                                                                       Messages.getMessage("selectsubjectpage.servererror.title"),
                                                                       JOptionPane.YES_NO_OPTION,
                                                                       JOptionPane.ERROR_MESSAGE,
                                                                       null,
                                                                       options,
                                                                       options[0]);
                            if (JOptionPane.NO_OPTION == n) {
                                throw new IOException(cause);
                            } else {
                                _logger.error("error getting subject list; retrying at user request", cause);
                            }
                        } else {
                            _logger.error("error getting subject list for " + _project, cause);
                            _logger.info("will retry in 1000 ms");
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ignore) {
                                JOptionPane.showMessageDialog(UploadAssistant.getInstance(),
                                                              Messages.getDialogFormattedMessage("error.generic.message", ignore.getMessage()),
                                                              Messages.getMessage("error.generic.title"),
                                                              JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        selectSubject(selection);
                    }
                });
                return selection;
            }
        };
        _project.submit(doRefresh);
        setCursor(Cursor.getDefaultCursor());
    }

    /**
     * Refreshes the list of subjects in the current project, re-selecting
     * the current selection after refresh is complete.
     */
    void refreshSubjectList() {
        refreshSubjectList(getSettings().getCurrentSubject());
    }

    /**
     * Tests whether the list contains the indicated {@link Subject subject}.
     *
     * @param list    The list to check.
     * @param subject The subject to check for.
     *
     * @return Returns true if the subject is in the list, false otherwise.
     */
    private static boolean contains(final JList<Subject> list, final Subject subject) {
        final ListModel<Subject> model = list.getModel();
        final int                size  = model.getSize();
        for (int index = 0; index < size; index++) {
            final Subject next = model.getElementAt(index);
            if (next.equals(subject)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the indicated subject as the selected value in the subject list.
     *
     * @param selection The subject to set as selected.
     */
    private void selectSubject(final Subject selection) {
        if (contains(_subjects, selection)) {
            _subjects.setSelectedValue(selection, true);
        } else {
            _subjects.setSelectedIndex(0);
        }
        _subjects.requestFocusInWindow();
    }

    private static final Logger _logger = LoggerFactory.getLogger(SelectSubjectPage.class);

    private final JList<Subject> _subjects;
    private final Dimension      _dimension;
    private       Project        _project;
}
