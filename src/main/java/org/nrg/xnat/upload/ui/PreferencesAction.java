/*
 * upload-assistant: org.nrg.xnat.upload.ui.PreferencesAction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import com.google.common.base.Function;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.nrg.xnat.upload.application.UploadAssistant;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.util.Arrays;

import static javax.swing.GroupLayout.Alignment.BASELINE;
import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

/**
 * This class displays the preferences dialog and changes any application settings as requested by the user.
 */
public class PreferencesAction extends AbstractAction {

    public static final String[] LOG_LEVELS = {"Off", "Trace", "Debug", "Info", "Warn", "Error"};

    public PreferencesAction(final UploadAssistant parent, final PropertyChangeListener listener, final MutableTriple<String, Boolean, File> logSettings) {
        super(Messages.getMessage("menu.preferences.title"));

        _parent = parent;
        _listener = listener;
        _logSettings = logSettings;

        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));
    }

    @Override
    public void actionPerformed(final ActionEvent event) {
        _log.info("Now performing the action {}", event.getActionCommand());
        PreferencesDialog.showDialog(_parent, _listener, _logSettings);
    }

    private static class PreferencesDialog extends AssistantDialog {
        public static void showDialog(final UploadAssistant parent, final PropertyChangeListener listener, final MutableTriple<String, Boolean, File> logSettings) {
            try {
                final PreferencesDialog dialog = new PreferencesDialog(parent, listener, logSettings);
                dialog.setModal(true);
                dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                dialog.setVisible(true);
            } catch (IllegalComponentStateException ignored) {
                // This happens every once in a while for no apparent reason. It doesn't seem to have any effect on function so just ignore it.
            }
        }

        private PreferencesDialog(final UploadAssistant parent, final PropertyChangeListener listener, final MutableTriple<String, Boolean, File> logSettings) {
            super(parent, Messages.getMessage("menu.preferences.title"), true);

            _parent = parent;
            _logSettings = logSettings;

            _logLevel = new JComboBox<>(LOG_LEVELS);
            _logPerformance = new JCheckBox(Messages.getMessage("preferences.logging.performance"));
            _logLocation = new JFileChooser() {{
                setControlButtonsAreShown(false);
                setMultiSelectionEnabled(false);
                setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                setAcceptAllFileFilterUsed(false);
                setFileHidingEnabled(true);
            }};

            _logLevel.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(final ItemEvent event) {
                    if (event.getStateChange() == ItemEvent.SELECTED) {
                        setLoggingPaneState();
                    }
                }
            });

            try {
                _logLevel.setSelectedItem(StringUtils.defaultIfBlank(logSettings.getLeft(), LOG_LEVELS[0]));
                _logPerformance.setSelected(BooleanUtils.toBooleanDefaultIfNull(logSettings.getMiddle(), false));
                _logLocation.setCurrentDirectory(resolveLoggingFolder(logSettings.getRight()));
                setLoggingPaneState();
            } catch (IOException e) {
                UIUtils.handleApplicationError(_parent, Messages.getMessage("preferences.logging.error", e.getMessage()));
            }

            _hasProxySpecified = getSettings().hasProxySpecified();
            _useProxy = new JCheckBox(Messages.getMessage("preferences.proxy.use.label"), _hasProxySpecified);
            _address = new JTextField();
            _port = new JFormattedTextField(PORT_FORMAT);
            _username = new JTextField();
            _password = new JPasswordField();

            final ImageIcon backgroundImage = new ImageIcon(getClass().getResource("/images/xnat-splash.png"));
            setSize(backgroundImage.getIconWidth(), backgroundImage.getIconHeight());

            final JTabbedPane tabs = new JTabbedPane();
            tabs.addTab(Messages.getMessage("preferences.tabs.preferences"), getPrefsPane(listener));
            tabs.addTab(Messages.getMessage("preferences.tabs.logging"), getLoggingPane());
            tabs.addTab(Messages.getMessage("preferences.tabs.proxy"), getProxyPane());

            add(new JPanel(new BorderLayout()) {{
                add(tabs, BorderLayout.NORTH);
                add(getButtonPanel(), BorderLayout.SOUTH);
            }});

            pack();
        }

        private void setLoggingPaneState() {
            final boolean isLoggingOff = _logLevel.getSelectedIndex() == 0;
            _logPerformance.setEnabled(!isLoggingOff);
            _logLocation.setEnabled(!isLoggingOff);
        }

        private File resolveLoggingFolder(final File configuredLogLocation) throws IOException {
            if (configuredLogLocation != null) {
                return configuredLogLocation;
            }
            final File cwd = getSettings().getCurrentDirectory();
            if (cwd != null) {
                return cwd;
            }
            return Files.createTempDirectory("upload-assistant").toFile();
        }

        private JPanel getPrefsPane(final PropertyChangeListener listener) {
            return new JPanel(new FlowLayout(FlowLayout.LEFT)) {{
                add(new JButton(Messages.getMessage("preferences.clear.label")) {{
                    addActionListener(new ClearPreferencesAction(PreferencesDialog.this, getSettings(), listener));
                }});
            }};
        }

        private JPanel getLoggingPane() {
            final JLabel levelLabel    = new JLabel(Messages.getLabel("preferences.logging.level"));
            final JLabel locationLabel = new JLabel(Messages.getLabel("preferences.logging.location"));

            levelLabel.setLabelFor(_logLevel);
            locationLabel.setLabelFor(_logLocation);

            final JPanel logging = new JPanel();
            logging.setLayout(new GridBagLayout());
            logging.add(levelLabel, new GridBagConstraints() {{
                gridx = gridy = 0;
                gridwidth = 1;
                weightx = 0.5;
                anchor = GridBagConstraints.LINE_START;
                insets = new Insets(5, 5, 5, 5);
            }});
            logging.add(_logLevel, new GridBagConstraints() {{
                gridx = 1;
                gridy = 0;
                gridwidth = 2;
                weightx = 0.5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                insets = new Insets(5, 5, 5, 5);
            }});
            logging.add(_logPerformance, new GridBagConstraints() {{
                gridx = 3;
                gridy = 0;
                gridwidth = 1;
                weightx = 0.5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                insets = new Insets(5, 5, 5, 5);
            }});
            logging.add(locationLabel, new GridBagConstraints() {{
                gridx = 0;
                gridy = 1;
                gridwidth = 1;
                weightx = 0.5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                insets = new Insets(20, 5, 5, 5);
            }});
            logging.add(_logLocation, new GridBagConstraints() {{
                gridx = 1;
                gridy = 1;
                gridwidth = 3;
                weightx = 0.5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                insets = new Insets(5, 5, 5, 5);
            }});
            return logging;
        }

        private JPanel getProxyPane() {
            final JLabel addressLabel  = new JLabel(Messages.getLabel("preferences.proxy.address"));
            final JLabel portLabel     = new JLabel(Messages.getLabel("preferences.proxy.port"));
            final JLabel usernameLabel = new JLabel(Messages.getLabel("preferences.proxy.username"));
            final JLabel passwordLabel = new JLabel(Messages.getLabel("preferences.proxy.password"));

            addressLabel.setLabelFor(_address);
            portLabel.setLabelFor(_port);
            usernameLabel.setLabelFor(_username);
            passwordLabel.setLabelFor(_password);

            final EnableProxyControlsFunction enable = new EnableProxyControlsFunction(_address, _port, _username, _password);
            enable.apply(_hasProxySpecified);
            _useProxy.addActionListener(new UseProxyActionListener(enable));
            if (_hasProxySpecified) {
                final Proxy             proxy        = getSettings().getProxy();
                final InetSocketAddress proxyAddress = (InetSocketAddress) proxy.address();
                _address.setText(proxyAddress.getHostName());
                _port.setText(Integer.toString(proxyAddress.getPort()));
                final String proxyAuth = getSettings().getProxyAuth();
                if (StringUtils.isNotBlank(proxyAuth)) {
                    final String[] atoms = proxyAuth.split(":");
                    _username.setText(atoms[0]);
                    _password.setText(atoms[1]);
                } else {
                    _username.setText("");
                    _password.setText("");
                }
            }

            final JPanel proxy = new JPanel();
            final GroupLayout layout = new GroupLayout(proxy) {{
                setAutoCreateContainerGaps(true);
            }};
            proxy.setLayout(layout);

            final GroupLayout.SequentialGroup horizontal = layout.createSequentialGroup();
            horizontal.addGroup(layout.createParallelGroup().addComponent(_useProxy).addComponent(addressLabel).addComponent(portLabel).addComponent(usernameLabel).addComponent(passwordLabel));
            horizontal.addGroup(layout.createParallelGroup().addComponent(_useProxy).addComponent(_address).addComponent(_port).addComponent(_username).addComponent(_password));
            layout.setHorizontalGroup(horizontal);

            final GroupLayout.SequentialGroup vertical = layout.createSequentialGroup();
            vertical.addGroup(layout.createParallelGroup(BASELINE).addComponent(_useProxy));
            vertical.addGroup(layout.createParallelGroup(BASELINE).addComponent(addressLabel).addComponent(_address));
            vertical.addGroup(layout.createParallelGroup(BASELINE).addComponent(portLabel).addComponent(_port));
            vertical.addGroup(layout.createParallelGroup(BASELINE).addComponent(usernameLabel).addComponent(_username));
            vertical.addGroup(layout.createParallelGroup(BASELINE).addComponent(passwordLabel).addComponent(_password));
            layout.setVerticalGroup(vertical);

            return proxy;
        }

        private JPanel getButtonPanel() {
            final JButton save = new JButton(Messages.getMessage("button.save")) {{
                addActionListener(new ActionListener() {
                    public void actionPerformed(final ActionEvent event) {
                        try {
                            final String  logLevel       = _logLevel.getItemAt(_logLevel.getSelectedIndex());
                            final File    logLocation    = StringUtils.equals(LOG_LEVELS[0], logLevel) ? null : _logLocation.getCurrentDirectory();
                            final Boolean logPerformance = !StringUtils.equals(LOG_LEVELS[0], logLevel) && _logPerformance.isSelected();

                            // Did they change the log level or location?
                            if (!StringUtils.equalsIgnoreCase(_logSettings.getLeft(), logLevel) || logLocation != null && !Files.isSameFile(logLocation.toPath(), _logSettings.getRight().toPath())) {
                                _logSettings.setLeft(logLevel);
                                _logSettings.setMiddle(logPerformance);
                                _logSettings.setRight(logLocation);
                                _parent.configureLogging();
                            }

                            if (_useProxy.isSelected()) {
                                try {
                                    getSettings().setProxy(UIUtils.formatProxyUrl(_address.getText(), _port.getText(), _username.getText(), _password.getPassword()));
                                } catch (URISyntaxException e) {
                                    JOptionPane.showMessageDialog(getParent(),
                                                                  Messages.getDialogFormattedMessage("preferences.proxy.error.message", e.getMessage()),
                                                                  Messages.getMessage("preferences.proxy.error.title"),
                                                                  JOptionPane.ERROR_MESSAGE);
                                }
                            } else {
                                getSettings().clearProxy();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            dispose();
                        }
                    }
                });
            }};

            final JButton cancel = new JButton(Messages.getMessage("button.cancel")) {{
                addActionListener(new ActionListener() {
                    public void actionPerformed(final ActionEvent event) {
                        dispose();
                    }
                });
            }};

            return new JPanel() {{
                setLayout(new FlowLayout(FlowLayout.RIGHT));
                add(save);
                add(cancel);
            }};
        }

        private final UploadAssistant                      _parent;
        private final MutableTriple<String, Boolean, File> _logSettings;
        private final boolean                              _hasProxySpecified;
        private final JCheckBox                            _useProxy;
        private final JComboBox<String>                    _logLevel;
        private final JCheckBox                            _logPerformance;
        private final JFileChooser                         _logLocation;
        private final JTextField                           _address;
        private final JTextField                           _port;
        private final JTextField                           _username;
        private final JPasswordField                       _password;
    }

    private static class EnableProxyControlsFunction implements Function<Boolean, Void> {
        private EnableProxyControlsFunction(final JTextField address, final JTextField port, final JTextField username, final JPasswordField password) {
            _address = address;
            _port = port;
            _username = username;
            _password = password;
        }

        @Override
        public Void apply(final Boolean enable) {
            _address.setEnabled(enable);
            _port.setEnabled(enable);
            _username.setEnabled(enable);
            _password.setEnabled(enable);
            return null;
        }

        private final JTextField     _address;
        private final JTextField     _port;
        private final JTextField     _username;
        private final JPasswordField _password;
    }

    private static class UseProxyActionListener implements ActionListener {
        private UseProxyActionListener(final Function<Boolean, Void> enable) {
            _enable = enable;
        }

        @Override
        public void actionPerformed(final ActionEvent event) {
            _enable.apply(((JCheckBox) event.getSource()).isSelected());
        }

        private final Function<Boolean, Void> _enable;
    }

    private static final Logger _log = LoggerFactory.getLogger(AboutAction.class);

    private static final NumberFormat PORT_FORMAT = NumberFormat.getIntegerInstance();

    static {
        PORT_FORMAT.setMaximumIntegerDigits(4);
        PORT_FORMAT.setGroupingUsed(false);
    }

    private final UploadAssistant                      _parent;
    private final PropertyChangeListener               _listener;
    private final MutableTriple<String, Boolean, File> _logSettings;
}
