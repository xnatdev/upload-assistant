/*
 * upload-assistant: org.nrg.xnat.upload.util.ArrayIterator
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<T> implements Iterator<T> {
	private final T[] ts;
	private int i = 0;
	
	public ArrayIterator(T...ts) {
		this.ts = ts;
	}
	
	public boolean hasNext() {
		return i < ts.length;
	}
	
	public T next() {
		if (i >= ts.length) {
			throw new NoSuchElementException();
		}
		return ts[i++];
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
