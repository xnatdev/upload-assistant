/*
 * upload-assistant: org.nrg.xnat.upload.io.Trawler
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;

import org.nrg.framework.io.EditProgressMonitor;
import org.nrg.xnat.upload.data.Session;

public interface Trawler {
	/**
	 * Identifies imaging sessions in the given group of files.  Files that are part of
	 * a session are removed from the underlying collection (so the Iterator must be one
	 * that implements remove().
	 * @param files 	   Iterator over files to be examined.
	 * @param remaining    The files remaining to be processed.
	 * @param pm    	   The progress monitor to be updated as files are processed.
	 * @return Any Sessions that could be identified
	 */
	Collection<Session> trawl(final Iterator<File> files, final Collection<File> remaining, EditProgressMonitor pm);
}
