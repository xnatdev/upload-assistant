/*
 * upload-assistant: org.nrg.xnat.upload.io.dcm.ZipSeriesUploader
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io.dcm;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.dcm4che2.data.Tag;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.xnat.upload.dcm.Series;
import org.nrg.xnat.upload.io.HttpUploadException;
import org.nrg.xnat.upload.io.HttpUtils;
import org.nrg.xnat.upload.io.UploadStatisticsReporter;
import org.nrg.xnat.upload.net.XnatServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.zip.ZipOutputStream;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

public class ZipSeriesUploader implements Callable<Set<String>> {
    public static final int MAX_TAG = Collections.max(ImmutableList.of(Tag.SOPInstanceUID, Tag.TransferSyntaxUID, Tag.FileMetaInformationVersion, Tag.SOPClassUID));

    public ZipSeriesUploader(final URL url,
                             final Series series,
                             final List<MizerContext> contexts,
                             final UploadStatisticsReporter progress) {
        _url = url;
        _series = series;
        _zipper = new SeriesZipper(contexts);
        _progress = progress;

        _currentXnatServer = getCurrentXnatServer();
    }

    public Set<String> call() throws HttpUploadException, IOException {
        return getSettings().isUseFixedSizeStreaming() ? sendFixedSize() : sendChunked();
    }

    public Series getSeries() {
        return _series;
    }

    public Set<String> sendFixedSize() throws HttpUploadException, IOException {
        final File zipFile = _zipper.buildSeriesZipFile(_series);
        try {
            final long zipSize = zipFile.length();
            final float zipRatio = (float) _series.getSize() / zipSize;

            logger.trace("creating connection to {}", _url);

            final HttpURLConnection connection = getRestServer().getNewConnection(_url, "POST", _currentXnatServer);
            connection.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_ZIP);
            connection.setFixedLengthStreamingMode(zipSize);
            connection.connect();

            try (final FileInputStream input = new FileInputStream(zipFile);
                 final OutputStream output = connection.getOutputStream()) {
                logger.trace("connection open; starting data send");
                final byte[] buf = new byte[BUF_SIZE];
                int chunk;
                for (int total = 0; (chunk = input.read(buf)) != -1; total += chunk) {
                    logger.trace("copying {} / {}", total, zipSize);
                    long adjustedChunkSize = (long) (chunk * zipRatio);
                    _progress.addSent(adjustedChunkSize);
                    output.write(buf, 0, chunk);
                }
                if (SUCCESS_CODES.contains(connection.getResponseCode())) {
                    logger.debug("series upload complete");
                    return Sets.newLinkedHashSet(HttpUtils.readEntityLines(connection));
                } else {
                    throw new HttpUploadException(connection);
                }
            } finally {
                connection.disconnect();
            }
        } finally {
            zipFile.delete();
        }
    }

    public Set<String> sendChunked() throws HttpUploadException, IOException {
        logger.warn("XNAT may not support chunked mode! After this fails, set fixed-size-streaming to true.");

        logger.trace("creating connection to {}", _url);
        final HttpURLConnection connection = getRestServer().getNewConnection(_url, "POST", _currentXnatServer);
        connection.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_ZIP);
        connection.setChunkedStreamingMode(0);
        if (logger.isDebugEnabled()) {
            logger.debug("Connecting to {}", _url);
            logger.debug("Request properties:");
            for (final Map.Entry<String, List<String>> entry : connection.getRequestProperties().entrySet()) {
                logger.debug("  {}", entry);
            }
        }
        connection.connect();

        logger.trace("connection open; starting data send");
        try (final ZipOutputStream zipOutputStream = new ZipOutputStream(connection.getOutputStream())) {
            for (final File file : _series.getFiles()) {
                _progress.addSent(file.length());
                logger.trace("sending {}", file);
                try {
                    _zipper.addFileToZip(file, zipOutputStream, _zipper.getStopTagInputHandler());
                } catch (MizerException e) {
                    logger.error("An exception occurred during anonymization of " + file.getAbsolutePath(), e);
                }
            }
            if (SUCCESS_CODES.contains(connection.getResponseCode())) {
                return Sets.newLinkedHashSet(HttpUtils.readEntityLines(connection));
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Response {} {} headers:", connection.getResponseCode(), connection.getResponseMessage());
                    for (final Map.Entry<String, List<String>> m : connection.getHeaderFields().entrySet()) {
                        logger.debug("  {}", m);
                    }
                }
                throw new HttpUploadException(connection);
            }
        } finally {
            connection.disconnect();
        }
    }

    private static final Set<Integer> SUCCESS_CODES       = ImmutableSet.of(HttpURLConnection.HTTP_OK, HttpURLConnection.HTTP_ACCEPTED);
    private static final int          BUF_SIZE            = 4096;
    private static final String       CONTENT_TYPE_HEADER = "Content-Type";
    private static final String       CONTENT_TYPE_ZIP    = "application/zip";

    private final Logger logger = LoggerFactory.getLogger(ZipSeriesUploader.class);

    private final URL                      _url;
    private final Series                   _series;
    private final SeriesZipper             _zipper;
    private final UploadStatisticsReporter _progress;
    private final XnatServer               _currentXnatServer;
}
