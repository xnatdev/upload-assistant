/*
 * upload-assistant: org.nrg.xnat.upload.io.dcm.SeriesZipper
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io.dcm;

import com.google.common.io.ByteStreams;
import org.dcm4che2.data.*;
import org.dcm4che2.io.*;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.service.MizerService;
import org.nrg.xnat.upload.dcm.Series;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.nrg.xnat.upload.application.UploadAssistant.getMizerService;

/**
 * @author Kevin A. Archie, karchie@wustl.edu
 */
@SuppressWarnings("Duplicates")
public class SeriesZipper {
    private final Logger logger = LoggerFactory.getLogger(SeriesZipper.class);

    private final MizerService        _mizer;
    private final List<MizerContext>  _contexts;
    private final StopTagInputHandler _stopTagInputHandler;

    public SeriesZipper(final List<MizerContext> contexts) {
        _mizer = getMizerService();
        _contexts = contexts;
        _stopTagInputHandler = makeStopTagInputHandler(contexts);
    }

    private static String removeCompressionSuffix(final String path) {
        return path.replaceAll("\\.[gG][zZ]$", "");
    }

    private StopTagInputHandler makeStopTagInputHandler(final List<MizerContext> scripts) {
        try {
            final Set<Integer> tags = _mizer.getScriptTags(scripts);
            return new StopTagInputHandler((tags.isEmpty() ? Tag.ScanningSequence : Collections.max(tags))+ 1);
        } catch (MizerException e) {
            return new StopTagInputHandler(Tag.PixelData - 1);
        }
    }

    public StopTagInputHandler getStopTagInputHandler() {
        return _stopTagInputHandler;
    }

    public void buildSeriesZipFile(final File zipFile, final Series series)
            throws IOException {
        logger.debug("creating zip file {}", zipFile);
        try (final ZipOutputStream output = new ZipOutputStream(new FileOutputStream(zipFile))) {
            logger.trace("adding {} files for series {}", series.getFileCount(), series);
            for (final File file : series.getFiles()) {
                try {
                    addFileToZip(file, output, getStopTagInputHandler());
                } catch (MizerException e) {
                    logger.warn("An error occurred anonymizing the file " + file.getAbsolutePath(), e);
                }
            }
        } catch (IOException e) {
            logger.trace("I/O exception building zip file", e);
        }
        logger.debug("zip file built");
    }

    public File buildSeriesZipFile(final Series series)
            throws IOException {
        final File zipFile = File.createTempFile("series", ".zip");
        try {
            buildSeriesZipFile(zipFile, series);
            return zipFile;
        } catch (IOException | RuntimeException | Error e) {
            //noinspection ResultOfMethodCallIgnored
            zipFile.delete();
            throw e;
        }
    }

    public void addFileToZip(final File file, final ZipOutputStream output, final DicomInputHandler handler) throws IOException, MizerException {
        final long remainder;

        try (final FileInputStream fileInputStream = new FileInputStream(file);
             final BufferedInputStream buffer = new BufferedInputStream(file.getName().endsWith(".gz") ? new GZIPInputStream(fileInputStream) : fileInputStream);
             final DicomInputStream dicomInputStream = new DicomInputStream(buffer)) {

            if (null != handler) {
                dicomInputStream.setHandler(handler);
            }

            final DicomObject    dicomObject    = dicomInputStream.readDicomObject();
            final TransferSyntax transferSyntax = TransferSyntax.valueOf(dicomObject.getString(Tag.TransferSyntaxUID, UID.ImplicitVRLittleEndian));
            if (transferSyntax.deflated() && null != handler) {
                // Can't do a simple binary copy with deflated transfer syntax.
                // Use no stop handler because we have to deserialize and serialize the entire object.
                try {
                    dicomInputStream.close();
                } catch (IOException ignore) {
                    // no biggie
                }
                addFileToZip(file, output, null);
                return;
            }

            final TransferSyntax transferSyntaxDeflated = transferSyntax.deflated() ? TransferSyntax.ExplicitVRLittleEndian : transferSyntax;

            output.putNextEntry(new ZipEntry(removeCompressionSuffix(file.getPath())));

            final DicomOutputStream dicomOutputStream = new DicomOutputStream(output);
            dicomOutputStream.setAutoFinish(false);          // Don't let DicomOutputStream finish the ZipOutputStream.

            final DicomObject outputDicomObject = new BasicDicomObject();
            outputDicomObject.initFileMetaInformation(dicomObject.getString(Tag.SOPClassUID), dicomObject.getString(Tag.SOPInstanceUID), transferSyntaxDeflated.uid());
            dicomOutputStream.writeFileMetaInformation(outputDicomObject);

            _mizer.anonymize(DicomObjectFactory.newInstance(dicomObject), _contexts);
            dicomOutputStream.writeDataset(dicomObject, transferSyntaxDeflated);

            if (null != handler) {
                buffer.reset();
                remainder = ByteStreams.copy(buffer, output);
            } else {
                remainder = 0;
            }
        } catch (DicomCodingException e) {
            logger.debug("error reading " + file, e);
            return;
        } catch (IOException e) {
            logger.warn("Error creating zip file", e);
            throw e;
        }

        output.closeEntry();
        logger.trace("added {}, {} bytes streamed", file, remainder);
    }
}
