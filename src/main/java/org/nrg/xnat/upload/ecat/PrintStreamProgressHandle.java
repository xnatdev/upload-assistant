/*
 * upload-assistant: org.nrg.xnat.upload.ecat.PrintStreamProgressHandle
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import org.netbeans.spi.wizard.ResultProgressHandle;

import java.awt.*;
import java.io.PrintStream;

class PrintStreamProgressHandle implements ResultProgressHandle {
    PrintStreamProgressHandle(final PrintStream outputStream) {
        _outputStream = outputStream;
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.ResultProgressHandle#addProgressComponents(java.awt.Container)
     */
    public void addProgressComponents(final Container panel) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.ResultProgressHandle#failed(java.lang.String, boolean)
     */
    public void failed(final String message, final boolean canNavigateBack) {
        _outputStream.println("upload FAILED: " + message);
        running = false;
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.ResultProgressHandle#finished(java.lang.Object)
     */
    public void finished(final Object result) {
        _outputStream.println("upload succeeded");
        running = false;
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.ResultProgressHandle#isRunning()
     */
    public boolean isRunning() {
        return running;
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.ResultProgressHandle#setBusy(java.lang.String)
     */
    public void setBusy(final String description) {
        _outputStream.println(description);
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.ResultProgressHandle#setProgress(int, int)
     */
    public void setProgress(final long currentStep, final long totalSteps) {
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.ResultProgressHandle#setProgress(java.lang.String, int, int)
     */
    public void setProgress(final String description, final long currentStep, final long totalSteps) {
        _outputStream.println(String.format("%s (%d/%d)", description, currentStep, totalSteps));
    }

    private final PrintStream _outputStream;
    private       boolean     running = true;
}
