/*
 * upload-assistant: org.nrg.xnat.upload.ecat.TextEcatVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import org.nrg.dicom.mizer.variables.Variable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class TextEcatVariable extends EcatSessionVariable implements DocumentListener {
	/**
	 * Creates a new instance for the indicated variable.
	 * @param variable    The variable for which an ECAT session variable should be created.
	 */
	public TextEcatVariable(final Variable variable) {
		super(variable);
		final Object v = variable.getValue();
        _text = new JTextField(null == v ? null : v.toString());
		_text.getDocument().addDocumentListener(this);
	}

	/* (non-Javadoc)
	 * @see org.nrg.xnat.upload.data.SessionVariable#getEditor()
	 */
	public Component getEditor() { return _text; }

	/* (non-Javadoc)
	 * @see org.nrg.xnat.upload.data.SessionVariable#refresh()
	 */
	public void refresh() {
		updateDisplayValue();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
	 */
	public void changedUpdate(final DocumentEvent e) {
		editTo(_text.getText());
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
	 */
	public void insertUpdate(final DocumentEvent e) {
		editTo(_text.getText());
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
	 */
	public void removeUpdate(final DocumentEvent e) {
		editTo(_text.getText());
	}

    private final JTextField _text;
}
