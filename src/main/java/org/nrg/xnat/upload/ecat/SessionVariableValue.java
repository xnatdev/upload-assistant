/*
 * upload-assistant: org.nrg.xnat.upload.ecat.SessionVariableValue
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.AbstractMizerValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.xnat.upload.data.SessionVariable;

public final class SessionVariableValue extends AbstractMizerValue {
    private final Set<Variable>   _variables;
    private final SessionVariable _variable;

    public SessionVariableValue(final SessionVariable source) {
        _variable = source;
        if (source instanceof EcatSessionVariable) {
            final EcatSessionVariable esv = (EcatSessionVariable) source;
            _variables = Collections.unmodifiableSet(esv.getVariables());
        } else {
            _variables = Collections.emptySet();
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() {
        return _variables;
    }

    @Override
    public void replace(final Variable variable) {
        //
    }

    @Override
    public SortedSet<Long> getTags() {
        return null;
    }

    @Override
    public String on(final DicomObjectI dicomObject) throws ScriptEvaluationException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Value#on(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    public String on(final Map<Integer, String> map) {
        final Value value = _variable.getValue();
        return value == null ? null : value.asString();
    }
}
