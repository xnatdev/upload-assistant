/*
 * upload-assistant: org.nrg.xnat.upload.ecat.IndexedSessionLabelFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.ecat.edit.AbstractIndexedLabelFunction;

public class IndexedSessionLabelFunction extends AbstractIndexedLabelFunction implements ScriptFunction {
    /**
     * This sets up an indexed session label function for the submitted sessions.
     *
     * @param sessions The sessions to process.
     */
    public IndexedSessionLabelFunction(final Future<Map<String, String>> sessions) {
        _sessions = sessions;
    }

    /**
     * This returns true in the case where the session label retrieval failed; this is
     * sort of broken but prevents an ugly infinite loop situation.
     *
     * @param label The label to test.
     *
     * @return Returns true if the label is available fo use.
     */
    @SuppressWarnings("Duplicates")
    @Override
    protected boolean isAvailable(String label) {
        try {
            final Map<String, String> m = _sessions.get();
            return !m.containsKey(label) && !m.containsValue(label);
        } catch (InterruptedException | ExecutionException e) {
            return true;
        }
    }

    private final Future<Map<String, String>> _sessions;
}
