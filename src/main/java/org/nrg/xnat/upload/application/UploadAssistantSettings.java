/*
 * upload-assistant: org.nrg.xnat.upload.application.UploadAssistantSettings
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.application;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.nrg.dicom.mizer.service.http.HttpClient;
import org.nrg.framework.constants.PrearchiveCode;
import org.nrg.framework.pinto.*;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.data.Session;
import org.nrg.xnat.upload.data.SessionVariable;
import org.nrg.xnat.upload.data.Subject;
import org.nrg.xnat.upload.net.XnatServer;
import org.nrg.xnat.upload.util.Messages;
import org.nrg.xnat.upload.util.VisitModalityI;
import org.nrg.xnat.upload.util.VisitOccurrenceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static java.net.Proxy.Type.HTTP;

/**
 * Contains command-line parameters and values.
 */
public class UploadAssistantSettings extends AbstractPintoBean {

    public static final String SERVERS             = "servers";
    public static final String SETTINGS            = "settings";
    public static final String CURRENT_XNAT_SERVER = "currentXnatServer";
    public static final String CURRENT_DIRECTORY   = "currentDirectory";
    public static final String CURRENT_LOCALE      = "locale";

    public UploadAssistantSettings(final Object parent, String[] arguments) throws PintoException {
        super(parent, arguments);

        final Preferences preferenceStore = Preferences.userNodeForPackage(UploadAssistant.class);
        _preferences = preferenceStore.node(SETTINGS);
        _serverStore = _preferences.node(SERVERS);

        _properties.putAll(loadApplicationProperties());
        _properties.putAll(loadPreferenceProperties());
        _properties.putAll(getPropertiesFromFiles());

        try {
            _servers.putAll(loadPersistedXnatServers());
        } catch (BackingStoreException e) {
            _log.error("An error occurred trying to access the Java Preferences backing store", e);
        }

        final String currentXnatServer = _preferences.get(CURRENT_XNAT_SERVER, null);
        _currentXnatServer = StringUtils.isNotBlank(currentXnatServer) ? _servers.get(currentXnatServer) : null;

        final String currentFolder = _preferences.get(CURRENT_DIRECTORY, null);
        if (StringUtils.isNotBlank(currentFolder)) {
            _currentDirectory = new File(currentFolder);
            if (!_currentDirectory.exists()) {
                _currentDirectory = null;
            }
        }

        final String locale = _preferences.get(CURRENT_LOCALE, null);
        Messages.setLocale(locale);
    }

    @Override
    public void validate() throws PintoException {
        final boolean hasProject = StringUtils.isNotBlank(_project);
        final boolean hasSubject = StringUtils.isNotBlank(_subject);
        if (!hasProject && hasSubject) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "You can't specify a subject without specifying a project.");
        }
        final boolean hasServer   = StringUtils.isNotBlank(_server);
        final boolean hasAddress  = StringUtils.isNotBlank(_address);
        final boolean hasUsername = StringUtils.isNotBlank(_username);
        final boolean hasPassword = StringUtils.isNotBlank(_password);
        if (hasProject && !(hasServer || (hasAddress && hasUsername && hasPassword))) {
            throw new PintoException(PintoExceptionType.SyntaxFormat, "You can't specify a project without specifying a server or server address, username, and password.");
        }

        if (StringUtils.isNotBlank(_log4jUrl)) {
            try {
                new URI(_log4jUrl);
            } catch (URISyntaxException e) {
                throw new PintoException(PintoExceptionType.Configuration, "The specified URL for the log4j configuration is not a valid URL: " + _log4jUrl);
            }
        }
    }

    public Object get(final String key) {
        return _state.get(key);
    }

    public void put(final String key, final Object value) {
        _state.put(key, value);
    }

    @Parameter(value = "j", longOption = "jsessionid", type = String.class, help = "Supplies a JSESSIONID cookie to allow the uploader to share an existing XNAT server session.")
    public void setJSessionId(final String jSessionId) {
        _jSessionId = jSessionId;
    }

    @Value("j")
    public String getJSessionId() {
        return _jSessionId;
    }

    @Parameter(value = "x", longOption = "server", type = String.class, help = "Supplies the name of an existing XNAT server definition to use as the default server.")
    public void setServer(final String server) {
        _server = server;
    }

    @Value("x")
    public String getServer() {
        return _server;
    }

    @Parameter(value = "a", longOption = "address", type = String.class, help = "Supplies the address of an existing XNAT server. If no username and password or JSESSIONID are specified, you'll need to specify these in the application.")
    public void setAddress(final String address) {
        _address = address;
    }

    @Value("a")
    public String getAddress() {
        return _address;
    }

    @Parameter(value = "u", longOption = "username", type = String.class, help = "Supplies the username for logging into an XNAT server. If no server address or password is specified, you'll need to specify these in the application.")
    public void setUsername(final String username) {
        _username = username;
    }

    @Value("u")
    public String getUsername() {
        return _username;
    }

    @Parameter(value = "pass", longOption = "password", type = String.class, help = "Supplies the password for logging into an XNAT server. If no server address or username is specified, you'll need to specify these in the application.")
    public void setPassword(final String password) {
        _password = password;
    }

    @Value("pass")
    public String getPassword() {
        return _password;
    }

    /**
     * Indicates the project to which data should be uploaded.
     *
     * @param project The ID of the project to which you want to upload data.
     */
    @Parameter(value = "p", longOption = "project", type = String.class, help = "Indicates the project to which data should be uploaded. You must specify the -x (--server) or -a (--address), -u (--username), and -p (--password) options when specifying a project.")
    public void setProject(final String project) {
        _project = project;
    }

    /**
     * Returns the project specified for data upload.
     *
     * @return The project specified for data upload.
     */
    @Value("p")
    public String getProject() {
        return _project;
    }

    /**
     * Indicates the subject to which data should be uploaded.
     *
     * @param subject The ID of the subject to which you want to upload data.
     */
    @Parameter(value = "s", longOption = "subject", type = String.class, help = "Indicates the subject to which data should be uploaded. You must specify the -p (--project) option along with its prerequisites when specifying this option.")
    public void setSubject(final String subject) {
        _subject = subject;
    }

    /**
     * Returns the subject specified for data upload.
     *
     * @return The subject specified for data upload.
     */
    @Value("s")
    public String getSubject() {
        return _subject;
    }

    @Parameter(value = "l", longOption = "label", type = String.class, help = "Specifies the session label to use for the uploaded session.")
    public void setPredefinedSessionLabel(final String sessionLabel) {
        _predefinedSessionLabel = sessionLabel;
    }

    @Value("l")
    public String getPredefinedSessionLabel() {
        return _predefinedSessionLabel;
    }

    @Parameter(value = "d", longOption = "date", type = Date.class, help = "Specifies the date to be used for the session validation.")
    public void setScanDate(final Date scanDate) {
        _scanDate = scanDate;
    }

    @Value("d")
    public Date getScanDate() {
        return _scanDate;
    }

    @Parameter(value = "vv", longOption = "visit", type = String.class, help = "Specifies the visit to use for the uploaded session.")
    public void setVisit(final String visit) {
        _visit = visit;
    }

    @Value("vv")
    public String getVisit() {
        return _visit;
    }

    @Parameter(value = "pr", longOption = "protocol", type = String.class, help = "Specifies the protocol to be validated for the uploaded session.")
    public void setProtocol(final String protocol) {
        _protocol = protocol;
    }

    @Value("pr")
    public String getProtocol() {
        return _protocol;
    }

    @Parameter(value = "m", longOption = "modality", type = String.class, help = "Specifies the modality expected for the session (note that this will restrict the sessions that you can locate and upload).")
    public void setExpectedModality(final String expectedModality) {
        _expectedModality = expectedModality;
    }

    @Value("m")
    public String getExpectedModality() {
        return _expectedModality;
    }

    @Parameter(value = "fss", longOption = "fixed-size-streaming", defaultValue = "true", help = "Specifies whether fixed-size streaming should be used for upload. This is true by default.")
    public void setUseFixedSizeStreaming(final boolean useFixedSizeStreaming) {
        _useFixedSizeStreaming = useFixedSizeStreaming;
    }

    @Value("fss")
    public boolean isUseFixedSizeStreaming() {
        return _useFixedSizeStreaming;
    }

    @Parameter(value = "t", longOption = "n-upload-threads", defaultValue = "4", help = "Specifies the number of upload threads to use. More threads can increase performance but also increase memory usage, which can cause the application to crash. The default value for this is 4.")
    public void setUploadThreads(final int uploadThreads) {
        _uploadThreads = uploadThreads;
    }

    @Value("t")
    public int getUploadThreads() {
        return _uploadThreads;
    }

    @Parameter(value = "lx", longOption = "use-logging-executor", argCount = ArgCount.StandAlone, help = "Specifies whether the logging executor service should be used. This is primarily for debugging and diagnostics purposes. By default, the standard non-logging executor service is used.")
    public void setUseLoggingExecutor(final boolean useLoggingExecutor) {
        _useLoggingExecutor = useLoggingExecutor;
    }

    @Value("lx")
    public boolean isUseLoggingExecutor() {
        return _useLoggingExecutor;
    }

    @Parameter(value = "l4j", longOption = "log4j-url", type = String.class, help = "Specifies a URL where the application can download an alternative version of the log4j configuration. This is primarily for debugging and diagnostics purposes. By default, the included logging configuration is used.")
    public void setLog4jUrl(final String log4jUrl) {
        _log4jUrl = log4jUrl;
    }

    @Value("l4j")
    public String getLog4jUrl() {
        return _log4jUrl;
    }

    @Parameter(value = "warn-on-dupes", longOption = "warn-on-dupe-session-labels", defaultValue = "true", help = "Indicates whether the user should be warned when the specified session label already exists in the target project. This setting defaults to true.")
    public void setWarnOnDupeSessionLabels(final boolean warnOnDupeSessionLabels) {
        _warnOnDupeSessionLabels = warnOnDupeSessionLabels;
    }

    @Value("warn-on-dupes")
    public boolean isWarnOnDupeSessionLabels() {
        return _warnOnDupeSessionLabels;
    }

    @Parameter(value = "allow-dupe-overwrites", longOption = "allow-overwrite-on-dupe-session-labels", defaultValue = "false", help = "Indicates whether uploads of sessions with duplicate labels should be uploaded with the overwrite flag turned on (this does not guarantee that the target XNAT server will allow the overwrite operation). This setting defaults to false.")
    public void setAllowOverwriteOnDupeSessionLabels(final boolean allowOverwriteOnDupeSessionLabels) {
        _allowOverwriteOnDupeSessionLabels = allowOverwriteOnDupeSessionLabels;
    }

    @Value("allow-dupe-overwrites")
    public boolean isAllowOverwriteOnDupeSessionLabels() {
        return _allowOverwriteOnDupeSessionLabels;
    }

    @Parameter(value = "allow-dupe-appends", longOption = "allow-append-on-dupe-session-labels", defaultValue = "true", help = "Indicates whether uploads of sessions with duplicate labels should be uploaded with the append flag turned on (this does not guarantee that the target XNAT server will allow the append operation).")
    public void setAllowAppendOnDupeSessionLabels(final boolean allowAppendOnDupeSessionLabels) {
        _allowAppendOnDupeSessionLabels = allowAppendOnDupeSessionLabels;
    }

    @Value("allow-dupe-appends")
    public boolean isAllowAppendOnDupeSessionLabels() {
        return _allowAppendOnDupeSessionLabels;
    }

    /**
     * Indicates a path to extended or override properties files.
     *
     * @param files Path(s) to one or more properties files.
     */
    @Parameter(value = "props", longOption = "properties", argCount = ArgCount.OneToN, help = "Indicates the location of one or more properties files that should be used to extend the available properties or override existing properties.")
    public void setPropertiesFilePaths(final List<String> files) {
        _fileSpecs.addAll(files);
    }

    /**
     * Returns the paths specified for properties files. Call {@link #getPropertiesFiles()} to get the files referenced
     * by these arguments or {@link #getPropertiesFromFiles()} to retrieve all of the properties from the specified
     * files.
     *
     * @return The paths specified for properties files.
     */
    @Value("props")
    public List<String> getPropertiesFilePaths() {
        return _fileSpecs;
    }

    /**
     * Indicates the address of a proxy server to use for HTTP connections. This should take the following form:
     *
     * <pre>{@code <user:pass@>server<:port>}</pre>
     *
     * Note that HTTP vs HTTPS protocols are not supported. If no port is specified, <b>80</b> is presumed. No
     * credentials are specified at all if none are provided on the proxy URL.
     *
     * @param proxyAddress The address to the proxy in the form described above.
     */
    @Parameter(value = "xx", longOption = "proxy", argCount = ArgCount.OneArgument, help = "Indicates the address of a proxy server to use for HTTP transactions.")
    public void setProxy(final String proxyAddress) throws URISyntaxException {
        _proxyAddress = proxyAddress;
        if (StringUtils.isNotBlank(proxyAddress)) {
            final URI uri = new URI(proxyAddress);
            _proxy = new Proxy(HTTP, new InetSocketAddress(uri.getHost(), uri.getPort() == -1 ? 80 : uri.getPort()));

            final String userInfo = uri.getUserInfo();
            _proxyAuth = StringUtils.isNotBlank(userInfo) ? "Basic " + HttpClient.getBase64Encoded(userInfo) : null;
        } else {
            _proxy = null;
            _proxyAuth = null;
        }

        // We do this as well so that any Mizer functions that use the HttpClient get the proxy settings as well.
        HttpClient.setProxy(_proxy);
        HttpClient.setProxyAuth(_proxyAuth);
    }

    /**
     * Returns the value specified for the proxy server on the
     *
     * @return The paths specified for properties files.
     */
    @Value("xx")
    public String getProxyAddress() {
        return _proxyAddress;
    }

    /**
     * Indicates whether a proxy has been specified for HTTP transactions. You can get a proxy instance
     *
     * @return Returns <b>true</b> if a proxy is specified, <b>false</b> otherwise.
     */
    public boolean hasProxySpecified() {
        return _proxy != null;
    }

    /**
     * Gets the configured proxy instance. If no proxy settings were specified, this method returns null.
     *
     * @return The configured proxy instance if specified.
     */
    public Proxy getProxy() {
        return _proxy;
    }

    /**
     * Gets the configured proxy authentication header value. If no proxy settings or user info were specified, this
     * method returns null.
     *
     * @return The configured proxy authentication header value if specified.
     */
    public String getProxyAuth() {
        return _proxyAuth;
    }

    /**
     * Clears all current proxy settings.
     */
    public void clearProxy() {
        _proxy = null;
        _proxyAuth = null;
    }

    /**
     * Gets file objects for each of the paths specified for the "-props" command-line parameter.
     *
     * @return A list of file objects.
     */
    public List<File> getPropertiesFiles() {
        if (_fileSpecs.size() > 0 && _files.size() == 0) {
            _files.addAll(Lists.transform(getPropertiesFilePaths(), new Function<String, File>() {
                @Override
                public File apply(final String path) {
                    if (StringUtils.isBlank(path)) {
                        return null;
                    }
                    return Paths.get(path).toFile();
                }
            }));
        }
        return _files;
    }

    /**
     * Processes all of the {@link #getPropertiesFiles()} files specified for the "-props" command-line parameter} and
     * loads them into a single properties object in the order specified. This means that properties in the the last
     * properties file specified will override any values set for properties with the same name.
     *
     * @return The properties and values from any property files specified for the "-props" command-line parameter.
     */
    public Properties getPropertiesFromFiles() {
        final Set<String> missing    = Sets.newHashSet();
        final Properties  properties = new Properties();
        for (final File file : getPropertiesFiles()) {
            try (final FileReader reader = new FileReader(file)) {
                properties.load(reader);
            } catch (FileNotFoundException e) {
                missing.add(file.getPath());
            } catch (IOException e) {
                _log.warn("An error occurred trying to read the file " + file.getPath(), e);
            }
        }
        if (missing.size() > 0) {
            _log.warn("The following properties files were specified using the -props command-line option but couldn't be found: {}", Joiner.on(", ").join(missing));
        }
        return properties;
    }

    /**
     * Gets the current subject. Returns null if the subject hasn't yet been set.
     *
     * @return The current subject if configured, null otherwise.
     */
    public Subject getCurrentSubject() {
        return _currentSubject;
    }

    /**
     * Sets the current subject to the indicated value.
     *
     * @param subject The subject to set as current.
     */
    public void setCurrentSubject(final Subject subject) {
        _currentSubject = subject;
    }

    /**
     * Gets the current session. Returns null if the session hasn't yet been set.
     *
     * @return The current session if configured, null otherwise.
     */
    public Session getCurrentSession() {
        return _currentSession;
    }

    /**
     * Sets the current session to the indicated value.
     *
     * @param session The session to set as current.
     */
    public void setCurrentSession(final Session session) {
        _currentSession = session;
    }

    /**
     * Indicates whether a server definition with the indicated name exists in the current store.
     *
     * @param name The name of the server to test for.
     *
     * @return Whether the specified server exists in the current store.
     */
    public boolean hasServer(final String name) {
        return _servers.containsKey(name);
    }

    /**
     * Returns the stored server names sorted alphabetically.
     *
     * @return The list of available server names.
     */
    public List<String> getXnatServerNames() {
        final List<String> serverNames = new ArrayList<>(_servers.keySet());
        Collections.sort(serverNames);
        return serverNames;
    }

    /**
     * Gets the currently selected server definition.
     *
     * @return The currently selected server definition.
     */
    public XnatServer getCurrentXnatServer() {
        return _currentXnatServer;
    }

    /**
     * Gets the server definition with the specified name.
     *
     * @param name The name of the server definition to retrieve.
     *
     * @return The specified server definition.
     */
    public XnatServer getXnatServer(final String name) {
        if (!hasServer(name)) {
            return null;
        }
        return _servers.get(name);
    }

    /**
     * Gets the server definitions.
     *
     * @return The specified server definition.
     */
    public List<XnatServer> getXnatServers() {
        return new ArrayList<>(_servers.values());
    }

    /**
     * Saves the submitted server in the Java Preferences store. If the server already exists, the existing definition
     * is overwritten. If it doesn't exist, a new definition will be written out. The newly saved server is set as the
     * {@link #getCurrentXnatServer() currently selected server}.
     *
     * @param server The server definition to be saved.
     */
    public void saveServer(final XnatServer server) {
        _servers.put(server.getName(), server);
        try {
            _serverStore.put(server.getName(), _objectMapper.writeValueAsString(server));
        } catch (JsonProcessingException e) {
            _log.error("An error occurred converting the server " + server.getName() + " to JSON", e);
        }
    }

    /**
     * Deletes the server definition with the specified name.
     *
     * @param name The name of the server definition to delete.
     */
    public void deleteServer(final String name) {
        if (_servers.containsKey(name)) {
            _servers.remove(name);
            if (StringUtils.isNotBlank(_serverStore.get(name, null))) {
                _serverStore.remove(name);
            }
            if (_currentXnatServer != null && StringUtils.equals(_currentXnatServer.getName(), name)) {
                _currentXnatServer = null;
            }
        }
    }

    /**
     * Sets the current locale for the application.
     *
     * @param locale The code for the locale to set.
     */
    public void setLocale(final Locale locale) {
        _preferences.put(CURRENT_LOCALE, locale.toLanguageTag());
        Messages.setLocale(locale);
    }

    /**
     * Returns the setting for the specified property. If there is no setting for the specific property, this method
     * returns null.
     *
     * @param property The name of the property to get.
     *
     * @return The value set for the property, null if not found.
     */
    public String getProperty(final String property) {
        return getProperty(property, null);
    }

    /**
     * Returns the setting for the specified property. If there is no setting for the specific property, the value for
     * the second parameter is returned instead.
     *
     * @param property     The name of the property to get.
     * @param defaultValue The value to return if the property isn't found or is null.
     *
     * @return The value set for the property or the specified default value if not found.
     */
    public String getProperty(final String property, final String defaultValue) {
        return _properties.getProperty(property, defaultValue);
    }

    /**
     * Sets the property to the specified value.
     *
     * @param property The name of the property to set.
     * @param value    The value to set for the property.
     */
    public void setProperty(final String property, final String value) {
        _properties.setProperty(property, value);
    }

    /**
     * Clears all currently saved preferences in the user's preference store.
     *
     * @throws BackingStoreException When an error occurs accessing the Java Preferences store.
     */
    public void clearSavedSettings() throws BackingStoreException {
        for (final String childName : _preferences.childrenNames()) {
            final Preferences child = _preferences.node(childName);
            child.clear();
        }
        _preferences.clear();
        _servers.clear();
    }

    /**
     * Loads properties from the default <b>META-INF/application.properties</b> configuration and any overrides
     * specified on the command line with the "-props" parameter.
     *
     * @return Returns the application properties.
     */
    private Properties loadApplicationProperties() {
        final Properties properties = new Properties();
        try (final InputStream input = getClass().getResourceAsStream("/META-INF/application.properties")) {
            if (input != null) {
                properties.load(input);
                _log.debug("Loaded {} properties from META-INF/application.properties");
            } else {
                _log.info("No resources found for META-INF/application.properties");
            }
        } catch (IOException exception) {
            _log.warn("Unable to access META-INF/application.properties resource for initialization", exception);
        }
        return properties;
    }

    /**
     * Loads properties from any stored Java preference settings. Note that this only looks at the top level of the
     * preferences store for this application.
     *
     * @return Properties loaded from the Java preference store.
     */
    private Properties loadPreferenceProperties() {
        final Properties properties = new Properties();
        try {
            if (_preferences.keys().length == 0) {
                _log.debug("Zero-length preferences store found, initializing.");
            } else {
                for (final String key : _preferences.keys()) {
                    final String value = _preferences.get(key, "");
                    properties.setProperty(key, value);
                }
            }
        } catch (BackingStoreException e) {
            _log.error("Unable to access the user preference settings", e);
        }
        return properties;
    }

    /**
     * Converts stored server URLs and usernames to a map of server information stored by server name.
     *
     * @return The map of stored XNAT server addresses.
     *
     * @throws BackingStoreException When an error occurs accessing the Java Preferences store.
     */
    private Map<String, XnatServer> loadPersistedXnatServers() throws BackingStoreException {
        final Map<String, XnatServer> servers = Maps.newHashMap();
        for (final String name : _serverStore.keys()) {
            final String content = _serverStore.get(name, "");
            try {
                final XnatServer server = _objectMapper.readValue(content, XnatServer.class);
                servers.put(server.getName(), server);
            } catch (JsonParseException e) {
                _log.error("Couldn't parse the server definition for " + name + ": " + content, e);
            } catch (JsonMappingException e) {
                _log.error("Bad data in the server definition for " + name + ": " + content, e);
            } catch (IOException e) {
                _log.error("There was an I/O error creating the server " + name + ": " + content, e);
            }
        }
        return servers;
    }

    /**
     * Updates the current server in the settings and the preferences store.
     *
     * @param server The server to set as current.
     */
    public void setCurrentXnatServer(final XnatServer server) {
        if (_currentXnatServer == null || !_currentXnatServer.equals(server)) {
            _currentXnatServer = server;
            _preferences.put(CURRENT_XNAT_SERVER, _currentXnatServer.getName());
        }
    }

    public File getCurrentDirectory() {
        return _currentDirectory;
    }

    public void setCurrentDirectory(final File currentDirectory) {
        _currentDirectory = currentDirectory;
        _preferences.put(CURRENT_DIRECTORY, currentDirectory.getAbsolutePath());
    }

    public Date getCurrentSessionDate() {
        return _currentSessionDate;
    }

    public void setCurrentSessionDate(final Date currentSessionDate) {
        _currentSessionDate = currentSessionDate;
    }

    public List<File> getSelectedFiles() {
        return _selectedFiles;
    }

    public void setSelectedFiles(final List<File> selectedFiles) {
        _selectedFiles = selectedFiles;
    }

    public PrearchiveCode getPrearchiveCodeOverride() {
        return _prearchiveCodeOverride;
    }

    public void setPrearchiveCodeOverride(final PrearchiveCode prearchiveCodeOverride) {
        _prearchiveCodeOverride = prearchiveCodeOverride;
    }

    public void setSelectedVisit(final VisitOccurrenceI selectedVisit) {
        _selectedVisit = selectedVisit;
    }

    public void clearSelectedVisit() {
        _selectedVisit = null;
    }

    public VisitOccurrenceI getSelectedVisit() {
        return _selectedVisit;
    }

    public boolean hasSelectedVisit() {
        return _selectedVisit != null;
    }

    public void setSelectedVisitModality(final VisitModalityI selectedVisitModality) {
        _selectedVisitModality = selectedVisitModality;
    }

    public void clearSelectedVisitModality() {
        _selectedVisitModality = null;
    }

    public VisitModalityI getSelectedVisitModality() {
        return _selectedVisitModality;
    }

    public boolean hasSelectedVisitModality() {
        return _selectedVisitModality != null;
    }

    public void setSessionVariables(final Map<String, SessionVariable> sessionVariables) {
        _sessionVariables = sessionVariables;
    }

    public Map<String, SessionVariable> getSessionVariables() {
        return _sessionVariables;
    }

    public void setSessionLabel(final SessionVariable sessionLabel) {
        _sessionLabel = sessionLabel;
    }

    public SessionVariable getSessionLabel() {
        return _sessionLabel;
    }

    private static final Logger _log = LoggerFactory.getLogger(UploadAssistantSettings.class);

    private final Preferences _preferences;
    private final Preferences _serverStore;

    private final ObjectMapper            _objectMapper = new ObjectMapper();
    private final Properties              _properties   = new Properties();
    private       Map<String, XnatServer> _servers      = Maps.newHashMap();
    private       Map<String, Object>     _state        = Maps.newHashMap();

    private final List<String> _fileSpecs = Lists.newArrayList();
    private final List<File>   _files     = Lists.newArrayList();

    private XnatServer                   _currentXnatServer;
    private File                         _currentDirectory;
    private String                       _jSessionId;
    private String                       _server;
    private String                       _address;
    private String                       _username;
    private String                       _password;
    private String                       _project;
    private String                       _subject;
    private String                       _predefinedSessionLabel;
    private Date                         _scanDate;
    private String                       _visit;
    private String                       _protocol;
    private String                       _expectedModality;
    private String                       _log4jUrl;
    private Project                      _currentProject;
    private Subject                      _currentSubject;
    private Session                      _currentSession;
    private Date                         _currentSessionDate;
    private List<File>                   _selectedFiles;
    private PrearchiveCode               _prearchiveCodeOverride;
    private VisitOccurrenceI             _selectedVisit;
    private VisitModalityI               _selectedVisitModality;
    private Map<String, SessionVariable> _sessionVariables;
    private SessionVariable              _sessionLabel;
    private String                       _proxyAddress;
    private Proxy                        _proxy;
    private String                       _proxyAuth;

    private boolean _warnOnDupeSessionLabels;
    private boolean _allowOverwriteOnDupeSessionLabels;
    private boolean _allowAppendOnDupeSessionLabels;
    private boolean _useFixedSizeStreaming;
    private int     _uploadThreads;
    private boolean _useLoggingExecutor;
}
