package org.nrg.xnat.upload.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"org.nrg.dicom.mizer.service.impl", "org.nrg.dcm.edit.mizer", "org.nrg.dicom.dicomedit.mizer"})
public class ApplicationConfig {
}
