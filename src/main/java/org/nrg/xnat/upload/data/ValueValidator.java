/*
 * upload-assistant: org.nrg.xnat.upload.data.ValueValidator
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

public interface ValueValidator {
    String getMessage(Object value);
    boolean isValid(Object value);
}
