/*
 * upload-assistant: org.nrg.xnat.upload.data.SessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import org.nrg.dicom.mizer.variables.Variable;

import java.awt.Component;

public interface SessionVariable extends Variable {
    String getValueMessage();

    String getLabel();

    boolean isMutable();

    SessionVariable fixValue();

    SessionVariable fixValue(String value) throws InvalidValueException;

    String validate(String value) throws InvalidValueException;

    void refresh();

    Component getEditor();

    void addListener(ValueListener listener);

    void removeListener(ValueListener listener);

    void addValidator(ValueValidator validator);

    void addShadow(SessionVariable shadow);

    class InvalidValueException extends RuntimeException {
        public InvalidValueException(final String message) {
            super(message);
        }

        public InvalidValueException(final SessionVariable variable, final String value) {
            this(buildMessage(variable, value));
        }

        public InvalidValueException(final String message, final Throwable cause) {
            super(message, cause);
        }

        public InvalidValueException(final SessionVariable variable, final String value, final Throwable cause) {
            this(buildMessage(variable, value), cause);
        }

        private static String buildMessage(final SessionVariable variable, final String value) {
            return "Invalid value for " + variable.getName() + ": " + value;
        }
    }
}
