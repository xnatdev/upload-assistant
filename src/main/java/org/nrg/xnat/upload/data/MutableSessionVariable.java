/*
 * upload-assistant: org.nrg.xnat.upload.data.MutableSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import org.nrg.dicom.mizer.values.Value;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


@SuppressWarnings("unused")
public class MutableSessionVariable extends AbstractSessionVariable implements DocumentListener {
    public MutableSessionVariable(final String name, final String value) {
        super(name);
        setValue(value);
        _editor = new JTextField(value);
        _editor.getDocument().addDocumentListener(this);
    }

    /**
     * Overrides the {@link AbstractSessionVariable#setValue(String) base setValue(String)} method to add updating the
     * display value in accordance with the change to the value.
     *
     * @param value The value to set.
     *
     * @return The previously set value of the variable.
     */
    @Override
    public String setValue(final String value) {
        super.setValue(value);
        return updateDisplayValue();
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
     */
    public final void changedUpdate(final DocumentEvent e) {
        update();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.SessionVariable#getEditor()
     */
    public final JTextField getEditor() {
        return _editor;
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
     */
    public final void insertUpdate(final DocumentEvent e) {
        update();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.SessionVariable#refresh()
     */
    public final void refresh() {
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
     */
    public final void removeUpdate(final DocumentEvent e) {
        update();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final Value value = getValue();
        return super.toString() + " " + getName() + " = " + (value == null ? "[null]" : value.asString());
    }

    private void update() {
        editTo(getEditor().getText());
    }

    private final JTextField _editor;
}
