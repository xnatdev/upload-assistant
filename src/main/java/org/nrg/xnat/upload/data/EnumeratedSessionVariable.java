/*
 * upload-assistant: org.nrg.xnat.upload.data.EnumeratedSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

public final class EnumeratedSessionVariable extends AbstractSessionVariable implements SessionVariable, ItemListener {
    public EnumeratedSessionVariable(final String name,
                                     final String exportField,
                                     final Collection<String> items,
                                     boolean requireSelection,
                                     boolean allowOther) {
        this(name, exportField, items, null, requireSelection, allowOther);
    }

    private EnumeratedSessionVariable(final String name,
                                      final String exportField,
                                      final Collection<String> items,
                                      final SessionVariable shadowed,
                                      boolean requireSelection,
                                      boolean allowOther) {
        super(name, exportField);
        this.shadowed = shadowed;
        this.requireSelection = requireSelection;
        this.allowOther = allowOther;
        setItems(items);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JComboBox getEditor() {
        return _comboBox;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Value getValue() {
        final String selectedItem = (String) _comboBox.getSelectedItem();
        final String value        = StringUtils.isBlank(selectedItem) || StringUtils.equals(MAKE_SELECTION, selectedItem) ? null : selectedItem;
        final Value  current      = super.getValue();
        if (current == null && value != null || current != null && !StringUtils.equals(current.asString(), value)) {
            super.setValue(value);
        }
        return super.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValueMessage() {
        final Object selectedItem = _comboBox.getSelectedItem();
        if (MAKE_SELECTION == selectedItem) {
            return "Select a value for " + getName();
        } else {
            final String v = _comboBox.getSelectedItem().toString();
            if (null == v || "".equals(v)) {
                return "Select a value for " + getName();
            } else {
                return null;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isHidden() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void itemStateChanged(final ItemEvent event) {
        if (StringUtils.equals(MAKE_SELECTION, _comboBox.getItemAt(0))) {
            _comboBox.removeItem(MAKE_SELECTION);
        }
        switch (event.getStateChange()) {
            case ItemEvent.DESELECTED:
                lastDeselected = event.getItem();
                if (UNSELECTABLES.contains(lastDeselected)) {
                    lastDeselected = null;
                }
                break;

            case ItemEvent.SELECTED:
                if (OTHER_ITEM == event.getItem()) {
                    final String value = JOptionPane.showInputDialog(Messages.getDialogFormattedMessage("enumeratedsessionvariable.entervalue", getName()));
                    if (null == value) {
                        if (null == lastDeselected) {
                            _comboBox.setSelectedIndex(0);
                        } else {
                            _comboBox.setSelectedItem(lastDeselected);
                        }
                    } else {
                        if (null == findItem(value)) {
                            _comboBox.addItem(value);
                            _comboBox.removeItem(OTHER_ITEM);
                            _comboBox.addItem(OTHER_ITEM);
                        }
                        _comboBox.setSelectedItem(value);
                    }
                }
        }
        fireHasChanged();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
        logger.debug("Now refreshing the variable {}", getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String setValue(final String value) {
        final String previous = (String) _comboBox.getSelectedItem();
        synchronized (this) {
            if (StringUtils.isNotBlank(value) && null == findItem(value)) {
                _comboBox.addItem(value);
            }
            _comboBox.setSelectedItem(value);
        }
        try {
            shadowed.setValue(value);
        } catch (InvalidValueException e) {
            logger.error("new value failed validation on shadowed variable " + shadowed, e);
        }
        fireHasChanged();
        return previous;
    }

    /**
     * This sets the list of items for the control and re-populates the list box accordingly.
     *
     * @param items The items to display in the list box.
     */
    public void setItems(final Collection<String> items) {
        // Only re-run the populateListBox() method if the new items list differs from the cached items list.
        // This allows control values to be more easily cached.
        if (this.items == null || !(this.items.containsAll(items) && items.containsAll(this.items))) {
            this.items = items;

            final Value        initial    = null == shadowed ? null : shadowed.getValue();
            final List<String> startItems = Lists.newArrayList();
            if (requireSelection) {
                startItems.add(MAKE_SELECTION);
                addValidator(new ValueValidator() {
                    public boolean isValid(final Object value) {
                        logger.trace("checking {} for validity", value);
                        return null != value;
                    }

                    public String getMessage(final Object value) {
                        return null == value ? "Select a value for " + getName() : null;
                    }
                });
            }
            startItems.addAll(items);
            if (allowOther) {
                startItems.add(OTHER_ITEM);
            }

            _comboBox.removeItemListener(this);
            _comboBox.removeAllItems();
            _comboBox.setModel(new DefaultComboBoxModel<>(new Vector<>(startItems)));
            _comboBox.setEditable(false);
            _comboBox.addItemListener(this);

            if (initial != null) {
                setValue(initial);
            }
        }
    }

    private Object findItem(final String value) {
        for (int i = 0; i < _comboBox.getItemCount(); i++) {
            if (_comboBox.getItemAt(i).equals(value)) {
                return _comboBox.getItemAt(i);
            }
        }
        return null;
    }

    private static final String        MAKE_SELECTION = "(Select)";
    private static final String        OTHER_ITEM     = "Other...";
    private static final Collection<?> UNSELECTABLES  = Arrays.asList(MAKE_SELECTION, OTHER_ITEM);
    private static final Logger        logger         = LoggerFactory.getLogger(EnumeratedSessionVariable.class);

    private final JComboBox<String> _comboBox = new JComboBox<>();

    private Object lastDeselected = null;

    private SessionVariable shadowed;
    private Collection<?>   items;
    private boolean         requireSelection;
    private boolean         allowOther;
}
