/*
 * upload-assistant: org.nrg.xnat.upload.data.UploadAbortedException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

public class UploadAbortedException extends Exception {
    private static final long serialVersionUID = 1L;

    public UploadAbortedException() {}

    public UploadAbortedException(String message) {
        super(message);
    }

    public UploadAbortedException(Throwable cause) {
        super(cause);
    }

    public UploadAbortedException(String message, Throwable cause) {
        super(message, cause);
     }
}
