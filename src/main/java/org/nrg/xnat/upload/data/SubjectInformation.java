/*
 * upload-assistant: org.nrg.xnat.upload.data.SubjectInformation
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.data;

import org.nrg.xnat.upload.net.StringResponseProcessor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public class SubjectInformation {
    public SubjectInformation(final Project project) {
        _project = project.toString();
        _uri = String.format(URL_TEMPLATE, project.toString());
    }

    public Subject uploadTo() throws UploadSubjectException {
        final StringResponseProcessor processor = createProcessor();
        try {
            getRestServer().doPost(_uri, processor);
        } catch (Exception e) {
            throw new UploadSubjectException("Error submitting new subject XML to XNAT.", e);
        }

        // parse out id from response
        return new Subject(_label, parseId(processor.toString()));
    }

    public void setLabel(String label) {
        _label = label;
    }

    public String getLabel() {
        return _label;
    }

    public String toString() {
        return getLabel();
    }

    protected Document buildXML() throws ParserConfigurationException {
        final DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        final Document document = builder.getDOMImplementation().createDocument("http://nrg.wustl.edu/xnat", "xnat:Subject", null);

        final Element root = document.getDocumentElement();
        root.setAttribute("project", _project);
        root.setAttribute("label", _label);
        return document;
    }

    private StringResponseProcessor createProcessor() throws UploadSubjectException {
        try {
            return new StringResponseProcessor(transformXML(buildXML()), MIME_TYPE, null, null);
        } catch (Exception e) {
            throw new UploadSubjectException("Error generating XML to create new subject.", e);
        }
    }

    private InputStream transformXML(Document document) throws TransformerException {
        // temporary buffer
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        // perform the transform
        final DOMSource source = new DOMSource(document);
        final StreamResult result = new StreamResult(out);
        final Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, result);

        // convert OutputStream to InputStream (could use
        // Piped{Input,Output}Stream, but would require spawning a thread, plus
        // we should have enough memory for the subject XML document)
        return new ByteArrayInputStream(out.toByteArray());
    }

    private String parseId(String response) {
        // response should contain a URI to the newly created subject, we take
        // the last part of that URI as the subject's ID
        String[] parts = response.trim().split("/");
        return parts[parts.length - 1];
    }

    public static final class UploadSubjectException extends Exception {
        private static final long serialVersionUID = -1331357997499624104L;

        public UploadSubjectException(String message, Throwable e) {
            super(message, e);
        }
    }

    private static final String URL_TEMPLATE = "/data/projects/%s/subjects?event_reason=Upload+Application";
    private static final String MIME_TYPE    = "text/xml";

    private final String _project;
    private final String _uri;

    private String _label;

}
