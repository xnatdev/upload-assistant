/*
 * upload-assistant: org.nrg.xnat.upload.data.SessionVariableNames
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

public final class SessionVariableNames {
    private SessionVariableNames() {
    }

    public static final String PROJECT        = "project";
    public static final String SUBJECT        = "subject";
    public static final String SESSION        = "session";
    public static final String SESSIONS       = "sessions";
    public static final String MODALITY_LABEL = "modalityLabel";
    public static final String VISIT_LABEL    = "*visit*";  //asterisks to avoid any problems with the anon script. this prevents visit from becoming a variable.
    public static final String PROTOCOL_LABEL = "*protocol*";  //asterisks to avoid any problems with the anon script. this prevents protocol from becoming a variable.
    public static final String TRACER         = "tracer";
    public static final String TRACER_PATH    = "xnat:petSessionData/tracer/name";
}
