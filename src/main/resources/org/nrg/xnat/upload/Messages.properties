#
# upload-assistant: /Users/rherrick/Development/XNAT/Tools/upload-assistant/src/main/resources/org/nrg/xnat/upload/Messages.properties
# XNAT http://www.xnat.org
# Copyright (c) 2017, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#

# suppress inspection "UnusedProperty" for whole file

application.name=XNAT Upload Assistant

messages.page.formatted=<html><p>{0}</p></html>
messages.dialog.formatted=<html><div style=''width: 400px;''>{0}</div></html>
labels.formatted=<html><strong>{0}:</strong></html>
labels.formatted.punctuated=<html><strong>{0}</strong></html>
link.formatted=<a href="{0}">{1}</a>
body.passthrough={0}
button.close=Close
button.save=Save
button.cancel=Cancel

xnatserverauthenticationpage.step=Log into XNAT
xnatserverauthenticationpage.description=Select an XNAT server that's already been defined or add credentials for a new server.
xnatserverauthenticationpage.authenticationfailed.message=Sorry, I couldn''t log you into {0} with the provided information. Please check your username and password and try again.
xnatserverauthenticationpage.authenticationfailed.title=Failed to login
xnatserverauthenticationpage.incomplete.message=Please enter the address for an XNAT server and valid credentials.
xnatserverauthenticationpage.ioerror.message=An unknown error occurred trying to access the specified server: {0}
xnatserverauthenticationpage.notanxnat.message=The server at "{0}" is responding but doesn''t seem to be an XNAT server.
xnatserverauthenticationpage.notanxnat.title=Not an XNAT Server
xnatserverauthenticationpage.systemerror.message=<p>An error occurred on the server at {0}. This is not a problem with the Upload Assistant \
  application but with the server. Please contact your system administrator with the following message:</p>\
  <p>{1}</p>
xnatserverauthenticationpage.systemerror.title=System Error
xnatserverauthenticationpage.unauthorized.message=Sorry, the server at {0} says that you don''t have permission to access it. Please check with the system administrator.
xnatserverauthenticationpage.unauthorized.title=Unauthorized
xnatserverauthenticationpage.unknown.message=<p>An unknown error occurred accessing the address "{0}":</p><p>{1}</p>
xnatserverauthenticationpage.unknown.title=Unknown error
xnatserverauthenticationpage.unsupported.message=<p>An unsupported operation was attempted on the server at the address "{0}":</p><p>{1}</p>
xnatserverauthenticationpage.unsupported.title=Unsupported operation
xnatserverauthenticationpage.sslprotocol.message=<p>There was an SSL protocol error accessing the address "{0}". This is usually due\
  to an issue with the server configuration. Please contact your system administrator with the following message:</p>\
  <p>{1}</p>
xnatserverauthenticationpage.sslprotocol.title=SSL Protocol Error
xnatserverauthenticationpage.unverified.message=<p>The specified server "{0}" supports HTTPS but uses an unverified SSL certificate.</p><p></p>\
  <p>You can allow this by checking the <b>Allow unverified certificates</b> option on the server definition. Note that this may expose sensitive \
  information if the connection has been compromised. Please check with your system administrator if you''re unsure how to proceed.</p>
xnatserverauthenticationpage.unverified.title=Unverified server

xnatserverauthenticationpanel.controls.create=Create a new server configuration.
xnatserverauthenticationpanel.controls.delete=Delete the currently selected server configuration.
xnatserverauthenticationpanel.controls.edit=Edit the currently selected server configuration.
xnatserverauthenticationpanel.controls.password=Password:
xnatserverauthenticationpanel.enterpassword=Enter a password and click Next to proceed.
xnatserverauthenticationpanel.selectserver=Please select a server from the drop-down list.

xnatserverconfigurationpanel.completeconfig=You must specify a valid server address and username.
xnatserverconfigurationpanel.controls.address=Server Address:
xnatserverconfigurationpanel.controls.address.tooltip=Enter the address for an XNAT server. If you don't specify a protocol (i.e. 'http://' or 'https://'), the application will try to figure out which to use.
xnatserverconfigurationpanel.controls.unverifiedssl=Allow unverified certificates
xnatserverconfigurationpanel.controls.unverifiedssl.tooltip=In most cases, you should leave this value unchecked! Only check this if you are accessing an XNAT server that uses a self-signed or other unverified SSL certificate. Please make sure you know what you are doing if you check this box!
xnatserverconfigurationpanel.controls.username=Username:
xnatserverconfigurationpanel.invalidserver.message=The specified address "{0}" is invalid. Please enter the address for an XNAT server and valid credentials.
xnatserverconfigurationpanel.invalidserver.title=Invalid server address
xnatserverconfigurationpanel.save=Save your server configuration to proceed.
xnatserverconfigurationpanel.saveorcancel=Save your server configuration or cancel if you don't want to create or update it.

selectprojectpage.noprojects.message=No accessible projects found on {0}

uploadresultpanel.success.withlabel={0} was successfully uploaded to the {1}.
uploadresultpanel.success.nolabel=The scan session was successfully uploaded to the {0}.
uploadresultpanel.dest.link=Click here to finish archiving the data.
uploadresultpanel.error.title=Error in URL
uploadresultpanel.error.message=An error occurred converting the URL {0} to a URI: {1}

uriclicklistener.error.title=Error Opening Page
uriclicklistener.error.message=An error occurred when the URI was clicked: {0}

error.generic.title=Error
error.generic.message=Got an error: {0}
error.nourlsupport.title=No URL Support
error.nourlsupport.message=Your system has no support for opening a URL in a web browser from a non-browser application. Copy and paste the URL below directly into a browser to view the uploaded session.
error.uploadfailure.title=Upload Failure
error.uploadfailure.message=<p>There was a problem uploading {0} ({1}) and I''ve already tried a total of {2,number,integer} times:</p><p>{3}</p><p>Should I try resending {0}?
error.uploadfailure.tryagain=Try again
error.uploadfailure.stop=Stop uploading this session
error.uploadfailure.failures=<ul>{0}</ul>
error.uploadfailure.failure=<li>{0}</li>
error.uploadfailure.items={0,number,integer} items, including:
error.exception.title=
error.exception.info.withmessage=<p>Found exception of type: {0}</p><blockquote>{1}</blockquote><pre>{2}</pre>
error.exception.info.withoutmessage=<p>Found exception of type: {0}</p><pre>{1}</pre>

error.http.notfound=<h3>Resource not found (404)</h3>\
  <p>The server at {0} is accessible but reports that the session resource {1} does not exist.</p>\
  <p>Contact your XNAT system administrators for help.</p>
error.http.internal=<h3>Internal Server Error (500)</h3>\
  <p>The server at {0} is accessible but was unable to commit the requested session due to an internal error.</p>\
  <p>Please contact your XNAT system administrators for help. A detailed description of the problem should be available in the DICOM receiver log or the XNAT logs.</p>
error.http.conflict=<h3>Session data conflict (409)</h3>\
  <p>The server at {0} reports a conflict between the uploaded data and a session in the archive.</p>\
  <p>All or part of this session was previously uploaded. Go to the prearchive page to archive the data just uploaded as a new session, or to merge it into an existing session.</p>
error.http.unknown=<h3>Unexpected error {0}: {1}</h3><p>Unable to commit uploaded session.</p><p>Please contact your XNAT system administrators for help.</p>
error.io.exception=<h3>Communications error</h3><p>The server at {0} is inaccessible ({1}). Please contact your XNAT system administrators for help.</p>
error.total.failure=<h3>Application Error</h3><p>An error in the uploader ({0}) prevented the session from being committed. Please contact your XNAT system administrators for help.</p>

menu.info.title=Info
menu.about.title=About {0}...
menu.locale.title=Choose locale
menu.preferences.title=Preferences...
menu.exit.title=Exit
about.message.name={0} version {1}
about.message.vendor={0}
about.message.build=Build # {0} ({1}), {2,date,medium} {2,time,short}
about.message.commit.unknown=Unknown
locale.message=Select the desired locale
preferences.tabs.preferences=Preferences
preferences.tabs.logging=Logging
preferences.tabs.proxy=Proxy
preferences.clear.label=Clear preferences
preferences.clear.message=Are you sure you want to clear all of your currently saved preference settings?
preferences.clear.error.title=An error occurred
preferences.clear.error.message=Something went wrong trying to clear your saved preferences. Please report the error message to your system administrator: {0}
preferences.logging.level=Level
preferences.logging.performance=Include performance metrics?
preferences.logging.location=Logs Folder
preferences.logging.error=An error occurred trying to configure the logging folder: {0}
preferences.proxy.use.label=Use HTTP/S proxy?
preferences.proxy.address=Address
preferences.proxy.port=Port
preferences.proxy.username=Username
preferences.proxy.password=Password
preferences.proxy.error.title=Invalid proxy settings
preferences.proxy.error.message=There was a problem setting the specified values for your proxy server. Please check the values and try again.

dialog.exit.title=Please Confirm
dialog.exit.message=Are you sure you want to close the {0} application?

vocabulary.archive=archive
vocabulary.prearchive=prearchive

assignsessionvariablespage.title=Enter session details
assignsessionvariablespage.description=Review session information and enter session details if applicable
assignsessionvariablespage.dup.session.id.title=Session ID Already Exists
assignsessionvariablespage.dup.session.id.message.with.overwrite=The session ID you've specified is already present in your project. Would you like to overwrite the existing session, append to the existing session, or provide a new session ID?
assignsessionvariablespage.dup.session.id.options.overwrite=Overwrite existing
assignsessionvariablespage.dup.session.id.options.append=Append to existing
assignsessionvariablespage.dup.session.id.options.new=Provide new session ID
assignsessionvariablespage.dup.session.id.message=The session ID you've specified is already present in your project. Would you like to append to the existing session, or provide a new session ID?
assignsessionvariablespage.dup.session.id.wo.append.message=The session ID you've specified is already present in your project. This project does not allow merging into existing sessions. You can specify a new session ID or consult the project manager.
assignsessionvariablespage.locked.session.id.wo.append.message=The session you've specified is already present in your project and the session is locked. You can specify a new session ID or consult the project manager.
assignsessionvariablespage.scan.conflict.session.id.wo.append.message=The session you've specified is already present in your project and contains scan data. You can specify a new session ID or consult the project manager.
assignsessionvariablespage.null.session.date.title=Session Has No Associated Date
assignsessionvariablespage.null.session.date.message=The session you selected has no date associated with it, but you specified a date for the session. This can happen when data has been anonymized or when scan data has been corrupted. Please verify that you've selected the correct session for upload. If not, click the <b>Prev</b> button to return to the previous screen to select a different session or click <b>Next</b> or <b>Finish</b> to continue with the existing session.
assignsessionvariablespage.invalid.session.date.title=Invalid Session Date/Time
assignsessionvariablespage.invalid.session.date.message=The date and/or time you indicated for your scan session does not match the date of the selected session. Please re-check the date for your new session and start over or click <b>Prev</b> and select another session that matches the indicated date.
assignsessionvariablespage.unexpected.modality.title=Unexpected Modality
assignsessionvariablespage.unexpected.modality.message=The expected modality does not match that of the session. Please re-check the modality of your new session and start over or click <b>Prev</b> and select another session that matches the indicated modality.

confirmsessiondatepage.title=Define visit details
confirmsessiondatepage.description=Enter the visit details of the session you plan to upload
confirmsessiondatapage.session.date.message=Please enter the date for the session you wish to upload. This date must match the session''s date or you will <i>not</i> be able to upload the session to the XNAT server. The date will be verified once you have selected the session you want to upload.
confirmsessiondatapage.session.date.message2=Please enter the date and time for the session you wish to upload. These values must match the session''s date or you will <i>not</i> be able to upload the session to the XNAT server. The date will be verified once you have selected the session you want to upload.
confirmsessiondatapage.session.date.label=Verify session date
confirmsessiondatapage.visit.label=Visit
confirmsessiondatapage.modality.label=Modality
confirmsessiondatapage.no.date.description=I don't know the date or my session doesn't have a date.
confirmsessiondatapage.session.date.problem=Enter a date for the session you intend to upload.
confirmsessiondatapage.visit.problem=Select a visit for the session.
confirmsessiondatapage.visit.modality.problem=Select a modality for the session.
confirmsessiondatapage.visit.ioexception.title=Network Error
confirmsessiondatapage.visit.ioexception.message=An error occurred while contacting the XNAT server at {}
confirmsessiondatapage.visit.jsonexception.title=Server Error
confirmsessiondatapage.visit.jsonexception.message=Received an invalid response from the XNAT server at {}

selectprojectpage.title=Select project
selectprojectpage.description=Select the project to which you will upload a session

selectsubjectpage.title=Select subject
selectsubjectpage.description=Select the subject for the session to be uploaded
selectsubjectpage.notfounderror.title=Not Found
selectsubjectpage.notfounderror.message=The XNAT server at {0} indicated that the selected project or its subjects couldn''t be found. The system may be down or otherwise inaccessible. Please try again in a few minutes.
selectsubjectpage.httperror.title=HTTP Error
selectsubjectpage.httperrorwithoutmessage.message=<p>An invalid HTTP status was returned from a server call:</p>\
  <table>\
  <tr><td style="font-weight: bold; valign: top;">Status:</td><td>{0}</td></tr>\
  <tr><td style="font-weight: bold; valign: top;">Operation:</td><td>{1}</td></tr>\
  </table>\
  <p>Please report this error to your XNAT system administrator.</p>
selectsubjectpage.httperrorwithmessage.message=<p>An invalid HTTP status was returned from a server call:</p>\
  <table>\
  <tr><td style="font-weight: bold; valign: top;">Status:</td><td>{0}</td></tr>\
  <tr><td style="font-weight: bold; valign: top;">Operation:</td><td>{1}</td></tr>\
  <tr><td style="font-weight: bold; valign: top;">Message:</td><td>{2}</td></tr>\
  </table>\
  <p>Please report this error to your XNAT system administrator.</p>
selectsubjectpage.networkerror.title=Network Error
selectsubjectpage.networkerror.message=Unable to contact {0}: {1}
selectsubjectpage.servererror.title=Server Error
selectsubjectpage.servererror.message=Received invalid response from {0}

newsubjectdialog.title=Create new subject
newsubjectdialog.button.create=Create
newsubjectdialog.label=Subject label
newsubjectdialog.message.empty=Label cannot be empty.
newsubjectdialog.message.invalid=Label may contain only letters, digits, hyphen (-) and underscore (_).
newsubjectdialog.message.exists=Subject {0} already exists.

newsubjectdialog.error.title=Subject creation failed
newsubjectdialog.error.message=There was a problem creating the new subject {0}: {1}

selectsessionpage.title=Verify selected session
selectsessionpage.description=Review and verify selected session information

selectfilespage.title=Select files
selectfilespage.description=Select the directory containing the session to be uploaded

ecatsession.selecttimezone=Select the time zone in which this scan was acquired

variables.project=Project
variables.subject=Subject
variables.session=Session
variables.sessions=Sessions
variables.modalityLabel=Modality
variables.visit=Visit
variables.protocol=Protocol
variables.tracer=Tracer

enumeratedsessionvariable.entervalue=Enter a value for {0}:

dicomsessionvariable.seriouserror.title=DICOM Script Error
dicomsessionvariable.seriouserror.message=<p>A serious error was encountered while preparing the DICOM. Please contact your site administrator with the following error message before uploading this data, as it could contain incorrect data including PHI or other sensitive information:</p><code>{0}</code>"
