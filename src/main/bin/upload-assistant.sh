#!/bin/sh
# upload-assistant
# Copyright (c) 2015 Washington University
# Author: Rick Herrick <jrherrick@wustl.edu>

# The following code attempts to locate the lib directory.
# It's probably preferable to just set the lib directory location explicitly:
# LIBDIR=/path/to/upload-assistant/lib
APP="`which $0`"
APPHOME="`dirname \"$APP\"`/.."
LIBDIR="${APPHOME}/lib"

if [ $JAVA_HOME ] ; then
	JAVA=${JAVA:-${JAVA_HOME}/bin/java}
else
	JAVA=${JAVA:-java}
fi

"$JAVA" -cp ${LIBDIR}/${project.artifactId}-${project.version}-jar-with-dependencies.jar ${mainClass} $*
